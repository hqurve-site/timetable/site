const webpack = require('webpack');
const { merge } = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

const common = require('./webpack.common.js');


module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  optimization: {
    // minimize: false,
  //   usedExports: true,
  //   providedExports: true,
  //   minimizer: [
  //     new TerserPlugin({
  //       minify: TerserPlugin.swcMinify
  //       // terserOptions: {
  //       //   ecma: 2016,
  //       //   module: true,
  //       // },
  //       
  //     }),
  //   ],
    minimizer: [
      `...`,
      new CssMinimizerPlugin(),
    ],
  },
  plugins: [
  ],
});
