
const child_process = require('child_process');

const INPUT = `${__dirname}/source`;
const OUTPUT= `${__dirname}/plain`;

const FORMATTING = [
  {
    modes: ['inkscape-clean'],
    files: [
      "arrow.svg",
      "circle.svg",
      "close.svg",
      "delete.svg",
      "error.svg",
      "icon_template.svg",
      "indicator-up-down.svg",
      "indicator-up.svg",
      "info.svg",
      "plus.svg",
      "search.svg",
      "warning.svg",
    ],
  },
  {
    modes: [
      'resize-16x16',
      'resize-32x32',
      'resize-64x64',
      'resize-128x128',
      'resize-256x256',
      'resize-256x',
    ],
    files: [
      'favicon.png',
    ],
  }
];

function disectFilename(file){
  let name = file.substring(0, file.lastIndexOf('.'));
  let ext = file.substring(file.lastIndexOf('.'));
  return {name, ext};
}

async function processFile(mode, file) {
  if (mode.startsWith('inkscape')) {
    if (mode === 'inkscape-clean') {
       await exec(`inkscape --export-filename '${OUTPUT}/${file}' --export-plain-svg '${INPUT}/${file}'`);
    }else {
      throw `Unknown mode ${mode}`;
    }
  }else if(mode.startsWith('resize-')) {
    let arg = mode.slice('resize-'.length);
    if (/^\d+x$/.test(arg)) {
      let size = arg;
      let {name, ext} = disectFilename(file);
      let outfile = `${name}-${size}${ext}`;
      await exec(`convert '${INPUT}/${file}' -resize ${size} '${OUTPUT}/${outfile}'`);
    }else if(/^\d+x\d+$/.test(arg)) {
      let size = arg;
      let {name, ext} = disectFilename(file);
      let outfile = `${name}-${size}${ext}`;
      // by emcconville https://stackoverflow.com/a/32466291
      await exec(`convert '${INPUT}/${file}' -resize ${size}^ -background None -gravity Center -extent ${size} '${OUTPUT}/${outfile}'`);
    }else{
      throw `Unknown mode ${mode}`;
    }
  }
}

async function main() {
  for (let {modes, files} of FORMATTING) {
    for (let file of files) {
      for (let mode of modes) {
        await processFile(mode, file);
      }
    }
  }
}

function exec(command, options={}) {
  options = Object.assign({
    cwd: __dirname,
  }, options);
  return new Promise((resolve, reject) => {
    console.log(command);
    let child = child_process.exec(command, options, (error, stdout, stderr) => {
      if (error) {
        reject(error);
      }else{
        resolve();
      }
    });
    child.stdout.pipe(process.stdout);
    child.stderr.pipe(process.stderr);
  });
}

main().then(() => process.exit());
