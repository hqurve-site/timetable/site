export { StaticDataAPI } from './static';

import { Container, ContainerId, Session, SessionId } from "..";
import { ContainerCheck, ContainerKey, SessionCheck, SessionKey } from "../keys";

export type SessionQuery = 
// sources
  { type: 'all' }
| {type: 'session-key', key: SessionKey}
| {type: 'container-key-pool', key: ContainerKey }

// checks
| {type: 'session-check', inner: SessionQuery, checks: SessionCheck[], negate?: boolean}

// merge
| {type: 'union', subs: SessionQuery[] }
| {type: 'intersection', subs: SessionQuery[] }
;

export type ContainerQuery = 
// sources
  { type: 'all' }
| {type: 'container-key', key: ContainerKey }
| {type: 'search', string: string }

// checks
| {type: 'container-check', inner: ContainerQuery, checks: ContainerCheck[], negate?: boolean}

// merge
| {type: 'union', subs: ContainerQuery[] }
| {type: 'intersection', subs: ContainerQuery[] }
;

export type LoadFlags = {
  load_referenced_objects?: boolean,
};
export type QueryFlags = {
  load_objects?: boolean,
  load_referenced_objects?: boolean,
};

export interface DataAPI {
  load_sessions(ids: Array<SessionId>, flags?: LoadFlags): Promise<Array<Session | undefined>>,
  load_containers(ids: Array<ContainerId>, flags?: LoadFlags): Promise<Array<Container | undefined>>,

  get_loaded_session(id: SessionId): Session | undefined
  get_loaded_container(id: ContainerId): Container | undefined

  query_sessions(query: SessionQuery, flags?: QueryFlags): Promise<Array<SessionId>>,
  query_containers(query: ContainerQuery, flags?: QueryFlags): Promise<Array<ContainerId>>,

  create_session_key(id: SessionId): Promise<SessionKey | undefined>,
  create_container_key(id: ContainerId): Promise<ContainerKey | undefined>,

  query_session_key(key: SessionKey, flags?: QueryFlags): Promise<SessionId | undefined>,
  query_container_key(key: ContainerKey, flags?: QueryFlags): Promise<ContainerId | undefined>,
}
