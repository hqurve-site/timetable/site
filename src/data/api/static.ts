import { ContainerQuery, DataAPI, SessionQuery } from '.';
import { Container, ContainerId, Session, SessionId } from '..';

import { Profile } from '../../globals';
import { LazyValue } from '../../util';
import { containerCheckMatches, ContainerKey, sessionCheckMatches, SessionKey, sessionKeyMatches, stringifyContainerKey } from '../keys';

type BundledData = {sessions: Session[], containers: Container[]};

export class StaticDataAPI implements DataAPI {
  private profile: Profile;

  private session_map: Map<SessionId, Session> = new Map();

  private container_map: Map<ContainerId, Container> = new Map();

  private container_key_map: Map<string, Container> = new Map();
  private sessions_without_containers: Session[] = [];

  private data: LazyValue<void>;

  constructor(profile: Profile, bundle: LazyValue<BundledData>) {
    this.profile = profile;
    this.data = bundle.then(timetable => {
      for (let s of timetable.sessions) this.session_map.set(s.id, s);
      for (let c of timetable.containers) {
        this.container_map.set(c.id, c);
        this.container_key_map.set(stringifyContainerKey(c), c);
      }

      for (let [, session] of this.session_map) {
        session.containers = session.containerIds!.map(cid => this.container_map.get(cid)!);
        if (session.containers.length === 0) {
          this.sessions_without_containers.push(session);
        }
      }
      for (let [, container] of this.container_map) {
        container.sessions = container.sessionIds!.map(sid => this.session_map.get(sid)!);
      }
    });
  }

  async create_session_key(id: SessionId): Promise<SessionKey | undefined> {
    let [_session] = await this.load_sessions([id]);
    if (_session === undefined) return undefined;
    let session = _session;
    
    return {
      type: session.type,
      day: session.day,
      timeperiod: {
        start: session.timeperiod.start,
        end: session.timeperiod.end,
      },
      tags: session.tags,
      containers: session.containers!.map(c => ({type: c.type, code: c.code})),
    };
  }
  async create_container_key(id: ContainerId): Promise<ContainerKey | undefined> {
    let [container] = await this.load_containers([id]);
    if (container === undefined) return undefined;
    return {
      type: container.type,
      code: container.code,
    };
  }
  async query_container_key(key: ContainerKey): Promise<ContainerId | undefined> {
    await this.data.load();
    return (await this.query_containers({type: 'container-key', key}))[0];
  }
  async query_session_key(key: SessionKey): Promise<SessionId | undefined> {
    await this.data.load();
    return (await this.query_sessions({type: 'session-key', key}))[0];
  }

  async load_sessions(ids: Array<SessionId>): Promise<Array<Session | undefined>> {
    await this.data.load();
    return ids.map(id => this.get_loaded_session(id));
  }
  async load_containers(ids: Array<ContainerId>): Promise<Array<Container | undefined>> {
    await this.data.load();
    return ids.map(id => this.get_loaded_container(id));
  }

  get_loaded_session(id: SessionId): Session | undefined {
      return this.session_map.get(id);   
  }
  get_loaded_container(id: ContainerId): Container | undefined {
      return this.container_map.get(id);   
  }

  private async _query_sessions(query: SessionQuery): Promise<Iterable<Session>> {
    if (query.type === 'all') {
      return this.session_map.values();
    } else if (query.type === 'session-key') {
      let container_ids = await Promise.all(query.key.containers.map(k => this.query_container_key(k)));

      let containers = [];
      for (let container_id of container_ids) {
        if (container_id === undefined) return [];
        containers.push(this.get_loaded_container(container_id)!);
      }

      let candidates: Session[];
      if (containers.length === 0) {
        candidates = this.sessions_without_containers;
      }else{
        candidates = [];
        let sets = containers.slice(1).map(c => new Set(c.sessionIds!));
        for (let session_id of containers[0].sessionIds) {
          if (sets.every(s => s.has(session_id))) {
            candidates.push(this.get_loaded_session(session_id)!);
          }
        }
      }
      return candidates.filter(s => sessionKeyMatches(query.key, s));
    } else if (query.type === 'container-key-pool') {
      let container_id = await this.query_container_key(query.key);
      if (container_id === undefined) return [];
      return this.get_loaded_container(container_id)!.sessions!;
    } else if (query.type === 'session-check') {
      let ret = [];
      let filter: (x: Session) => boolean;
      
      if (query.negate === true) {
        filter = session => !query.checks.some(c => sessionCheckMatches(c, session));
      }else{
        filter = session => query.checks.some(c => sessionCheckMatches(c, session));
      }
      for (let session of await this._query_sessions(query.inner)) {
        if (filter(session)) ret.push(session);
      }
      return ret;
    } else if (query.type === 'union') {
      let ret: Set<Session> = new Set();
      for (let q of query.subs) {
        for (let session of await this._query_sessions(q)) ret.add(session);
      }
      return ret;
    } else if (query.type === 'intersection') {
      if (query.subs.length === 0) return [];
      let ret: Set<Session> = new Set(await this._query_sessions(query.subs[0]));
      for (let q of query.subs.slice(1)) {
        let _ret: Set<Session> = new Set();
        for (let session of await this._query_sessions(q)) {
          if (ret.has(session)) _ret.add(session);
        }
        ret = _ret;
      }
      return ret;
    } else {
      throw `unsupported query: ${query}`;
    }
  }
  private async _query_containers(query: ContainerQuery): Promise<Iterable<Container>> {
    if (query.type === 'all') {
      return this.container_map.values();
    } else if (query.type === 'container-key') {
      let container = this.container_key_map.get(stringifyContainerKey(query.key));
      if (container === undefined) return [];
      return [container];
    } else if (query.type === 'search') {
      let containers = [];
      container_loop:
      for (let container of this.container_map.values()) {

        let haystack = container.type + container.code + (container.name ?? "");
        haystack = haystack.replace(/\s+/, '').toLowerCase();
        for (let word of query.string.toLowerCase().split(/\s+/)) {
          if (!haystack.includes(word)) continue container_loop;
        }
        containers.push(container);
      }
      return containers;
    } else if (query.type === 'container-check') {
      let ret = [];
      let filter: (x: Container) => boolean;
      
      if (query.negate === true) {
        filter = container => !query.checks.some(c => containerCheckMatches(c, container));
      }else{
        filter = container => query.checks.some(c => containerCheckMatches(c, container));
      }
      for (let container of await this._query_containers(query.inner)) {
        if (filter(container)) ret.push(container);
      }
      return ret;
    } else if (query.type === 'union') {
      let ret: Set<Container> = new Set();
      for (let q of query.subs) {
        for (let container of await this._query_containers(q)) ret.add(container);
      }
      return ret;
    } else if (query.type === 'intersection') {
      if (query.subs.length === 0) return []; // define empty intersection to be empty

      let ret: Set<Container> = new Set(await this._query_containers(query.subs[0]));
      for (let q of query.subs.slice(1)) {
        let _ret: Set<Container> = new Set();
        for (let container of await this._query_containers(q)) {
          if (ret.has(container)) _ret.add(container);
        }
        ret = _ret;
      }
      return ret;
    } else {
      throw `unsupported query: ${query}`;
    }
  }
  async query_sessions(query: SessionQuery): Promise<Array<SessionId>> {
    await this.data.load();
    
    return [... await this._query_sessions(query)].map(s => s.id);
  }
  async query_containers(query: ContainerQuery): Promise<Array<ContainerId>> {
    await this.data.load();

    return [... await this._query_containers(query)].map(c => c.id);
  }
}
