/**
 * If you want to hold a reference to a container over a long period of time (ie for serialization),
 * use this.
 * 
 * To get the container which it references, call the resolve method.
 * If it is found, the container is returned as well as any changes since the reference's creation.
 * 
 * Note:
 * Changes are relative to the last save point. (saved internally as the container keys and respective session keys).
 * To update the save point, the trigger_change method should be used.
 * It is useful to only call the trigger_change method when the end user has acknowledged the changes present.
 */

import { Container, SessionId } from "..";
import { Profile } from "../../globals";
import { KeyedPromise } from "../../util";
import { ContainerKey, SessionKey, sessionKeyEquals } from "../keys";

export type ContainerResponse =
  { status: "found",
    container: Container,
    sessions: Array<{id: SessionId, key: SessionKey}>,
    removedSessions: SessionKey[],
    addedSessions: SessionKey[],
    trigger_change: () => void,
  }
| { status: "missing" }
;

export type SeriableContainerReference =
  { version: "1",
    inner: {
      key: ContainerKey,
      session_keys: SessionKey[],
    },
  }
;

export class ContainerReference{
  private inner: {
    key: ContainerKey,
    session_keys: SessionKey[],
  };
  private promise: KeyedPromise<Profile['id'], ContainerResponse>;

  get key() {
    return this.inner.key;
  }
  get session_keys() {
    return [...this.inner.session_keys];
  }

  private constructor(key: ContainerKey, session_keys: SessionKey[]) {
    this.inner = {
      key, session_keys
    };
    this.promise = new KeyedPromise();
  }

  static async new(key: ContainerKey, profile: Profile): Promise<ContainerReference | undefined> {
    let container_id = await profile.dataAPI.query_container_key(key, {load_objects: true});
    if (container_id === undefined) return undefined;

    let container = profile.dataAPI.get_loaded_container(container_id)!;
    let session_keys = await Promise.all(container.sessionIds.map(async id => (await profile.dataAPI.create_session_key(id))! ));

    return new ContainerReference(key, session_keys);
  }

  static deserialize(ref: SeriableContainerReference): ContainerReference {
    if (ref.version === "1") {
      return new ContainerReference(ref.inner.key, [...ref.inner.session_keys]);
    }else{
      throw `Invalid ref ${ref}`;
    }
  }

  serialize(): SeriableContainerReference {
    return {
      version: "1",
      inner: {
        key: this.inner.key,
        session_keys: [...this.inner.session_keys],
      },
    };
  }

  resolve(profile: Profile): Promise<ContainerResponse> { 
    return this.promise.run(profile.id, async (trigger_change) => {
      let container_id = await profile.dataAPI.query_container_key(this.inner.key, {load_objects: true, load_referenced_objects: true})
      if (container_id === undefined) return { status: "missing" };
      let container = profile.dataAPI.get_loaded_container(container_id)!;

      let old_sessions = await Promise.all(this.inner.session_keys.map(async key => {
        let id = await profile.dataAPI.query_session_key(key);
        return {id, key: key};
      }));

      let new_sessions = await Promise.all(container.sessionIds.map(async id => {
        let key = await profile.dataAPI.create_session_key(id);
        return {id, key: key!};
      }));

      let addedSessions = new_sessions.filter(({key}) => !old_sessions.some(v => sessionKeyEquals(key, v.key))).map(({key}) => key);
      let removedSessions = old_sessions.filter(({key}) => !new_sessions.some(v => sessionKeyEquals(key, v.key))).map(({key}) => key);

      return {
        status: 'found',
        sessions: new_sessions,
        container,
        addedSessions,
        removedSessions,
        trigger_change,
      };
    }, res => {
      if (res.status === 'missing') return {status: 'missing'};
      this.inner.session_keys = res.sessions.map(({key}) => key).filter((k): k is SessionKey => k !== undefined);
      return {
        status: 'found',
        sessions: res.sessions,
        container: res.container,
        addedSessions: [],
        removedSessions: [],
        trigger_change(){},
      }
    });
  }
}
