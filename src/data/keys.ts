import { Container, Session } from ".";

// keys are guaranteed to match at most one object

export type SessionKey = {
  type: string,
  day: number,
  timeperiod: {start: number, end: number},
  tags: { [index: string]: string[] },
  containers: Array<ContainerKey>,
};
export type SessionCheck = Omit<Partial<SessionKey>, 'containers'> & {containers: Array<ContainerCheck>};

export type ContainerKey = {
  type: string,
  code: string,
};
export type ContainerCheck = Partial<ContainerKey>;


export function stringifyContainerKey(k: ContainerKey): string {
  return `${k.type}__${k.code}`;
}
export function parseContainerKey(k: string): ContainerKey | undefined {
  let split = k.split('__', 2);

  if (split.length === 2) {
    return {
      type: split[0],
      code: split[1],
    }
  }
}

export function containerKeyMatches(a: ContainerKey, b: Container): boolean {
  return containerCheckMatches(a, b);
}
export function containerKeyEquals(a: ContainerKey, b: ContainerKey): boolean {
  return a.code === b.code && a.type === b.type;
}

export function containerCheckMatches(a: ContainerCheck, b: Container, overrides: {[K in keyof ContainerCheck]?: boolean} = {}): boolean {
  if ((overrides.type ?? true) && a.type !== undefined && a.type !== b.type) return false;
  if ((overrides.code ?? true) && a.code !== undefined && a.code !== b.code) return false;
  return true;
}

export function sessionKeyMatches(a: SessionKey, b: Session): boolean {
  if (!sessionCheckMatches(a, b)) return false;

  // only need to check equality of length since we have already checked subsets
  for (let [t, ta] of Object.entries(a.tags)) {
    let tb = b.tags[t] ?? [];
    if (ta.length !== tb.length) return false;
  }
  if (a.containers.length !== (b.containers?.length ?? 0)) return false;

  return true;
}

export function sessionCheckMatches(a: SessionCheck, b: Session, overrides: {[K in keyof SessionCheck]?: boolean} = {}): boolean {
  if ((overrides.type ?? true) && a.type !== undefined && a.type !== b.type) return false;
  if ((overrides.day ?? true) && a.day !== undefined && a.day !== b.day) return false;
  if ((overrides.timeperiod ?? true) && a.timeperiod !== undefined){
    if ( a.timeperiod.start !== b.timeperiod.start
      || a.timeperiod.end !== b.timeperiod.end
    ) return false;
  }

  if ((overrides.tags ?? true) && a.tags !== undefined) {
    for (let [t, ta] of Object.entries(a.tags)) {
      let tb = b.tags[t] ?? [];
      if (!ta.every(x => tb.includes(x))) return false;
    }
  }
  if ((overrides.containers ?? true) && a.containers !== undefined) {
    for (let c of a.containers) {
      if (!(b.containers ?? []).some(x => containerCheckMatches(c, x))) return false;
    }
  }
  
  return true;
}
export function sessionKeyEquals(a: SessionKey, b: SessionKey): boolean {
  if (a.type !== b.type) return false;
  if (a.day !== b.day) return false;
  if ( a.timeperiod.start !== b.timeperiod.start
    || a.timeperiod.end !== b.timeperiod.end
  ) return false;

  for (let t of new Set([...Object.keys(a.tags),...Object.keys(b.tags)])) {
    let ta = a.tags[t] ?? [];
    let tb = b.tags[t] ?? [];
    if (ta.length !== tb.length) return false;
    if (!ta.every(x => tb.includes(x))) return false;
  }

  if (a.containers.length !== b.containers.length) return false;
  if (!a.containers.every(x => b.containers.some(y => containerKeyEquals(x,y)))) return false;

  return true;
}
