export type SessionId = number;
export type ContainerId = number;

export type Session = {
  id: SessionId,
  last_modified_timestamp: number | null,
  enabled: Boolean,
  type: string,
  day: number,
  timeperiod: {start: number, end: number},
  tags: { [index: string]: string[] },
  weeks: number[],
  containerIds: ContainerId[],
  containers?: Container[],
};

export type Container = {
  id: ContainerId,
  last_modified_timestamp: number | null,
  enabled: Boolean,
  type: string,
  code: string,
  name: string | null,
  sessionIds: SessionId[],
  sessions?: Session[]
};


