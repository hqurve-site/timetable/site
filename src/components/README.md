# Components listing

Components are typically created with a specialized config and optional placement configuration.
The generated class object has a public `div` field which may be used to attach/remove the component from the DOM after construction.

- [agenda](./agenda/index.ts): this is what is used to build the timetable. Multiple of these are used on the builder site
- [buffered-table and paged-table](./buffered-table): simple tables to display a table of items. The entries can be sorted by column.
    - [BufferedTable](./buffered-table/buffered-table.ts) is infinitely scrollable
    - [PagedTable](./buffered-table/paged-table.ts) is paged
- [button-naviagtor](./button-naviagator/index.ts) is used in the PagedTable to select pages. It has the ability to
    - Only show a fixed width of numbers
    - arrows for the next, previous, first and last elements 
- [button-select](./button-select/index.ts) is used to select multiple options at once (eg used to select categories for filtering).
- [query-builder](./query-builder/index.ts) is used to build queries. It has internal mechanisms for switching the type and setting the name.
- [reorderable-list](./reorderble-list/index.ts) shows a list of items which are able to be reordered using drag and drop.
- [select-list](./select) shows a dropdown list for selecting an item.
- [timetable](./timetable) shows a timetable
