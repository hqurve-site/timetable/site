import { ColumnConfig, StdColumnConfig } from ".";
import { createElement } from "../../util";


export function standardizeColumnConfig<T, K extends keyof T & string>(col: ColumnConfig<T, K>): StdColumnConfig<T> {
  let name = col.name;
  let title;
  let cellFactory;
  let sort;
  let formatter = col.cellFormatter ?? (() => {});

  if ("cellFactory" in col) {
    title = col.title;
    cellFactory = (obj: T) => {
      let cell = col.cellFactory(obj);
      formatter(obj, cell);
      return cell;
    };
  }else if("elementFactory" in col) {
    title = col.title;
    cellFactory = (obj: T) => {
      let cell = createElement('div');
      let element = col.elementFactory(obj);
      cell.append(element);
      formatter(obj, cell);
      return cell;
    };
  }else if("property" in col) {
    title = col.title ?? col.property;

    cellFactory = (obj: T) => {
      let cell = createElement('div');
      cell.innerText = "" + obj[col.property];
      formatter(obj, cell);
      return cell;
    };
  }else{
    title = col.title;
    cellFactory = (obj: T) => {
      let cell = createElement('div');
      cell.innerText = col.getter(obj);
      formatter(obj, cell);
      return cell;
    };
  }

  if (col.sort !== undefined) {
    let _sort = col.sort;
    sort = {
      forward: _sort,
      reverse: (a: T, b: T) => _sort(b,a),
    };
  }

  return {name, title, cellFactory, sort};
}
