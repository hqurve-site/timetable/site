
export type SortFunction<T> = (a: T, b: T) => number;

// // by jcalz https://stackoverflow.com/a/61765012
// type KeysMatching<T, V> = { [K in keyof T]: T[K] extends V ? K : never }[keyof T]

export type ColumnConfig<T, K extends keyof T> = {
  name: string,
  sort?: SortFunction<T>,
  cellFormatter?: (obj: T, cell: HTMLElement) => void,
} & (
  { title: string, cellFactory(obj: T): HTMLElement }
  | { title: string, elementFactory(obj: T): Node | string }
  | { title?: string, property: K }
  | { title: string, getter(obj: T): string }
);

export type StdColumnConfig<T> = {
  name: string,
  sort?: { forward: SortFunction<T>, reverse: SortFunction<T> },
  title: string,
  cellFactory(obj: T): HTMLElement,
}

export {default as SimpleTable} from './simple-table';


