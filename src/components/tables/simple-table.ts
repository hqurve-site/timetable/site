import './simple-table.scss';

import { ColumnConfig, SortFunction, StdColumnConfig } from ".";
import { createElement, PlacementOptions, removeChildren } from "../../util";
import { standardizeColumnConfig } from "./common";

type Config<Id, T extends {id: Id}> = {
  columns: Array<ColumnConfig<T, keyof T & string>>,
  fallbackSort: SortFunction<T>,
  rowFormatter?: (obj: T, row: HTMLElement) => void,
};

export default class SimpleTable<Id, T extends {id: Id}> {
  div: HTMLElement;
  
  private columns: Array<StdColumnConfig<T>>;
  private rowFormatter: (obj: T, row: HTMLElement) => void;

  private elements: {
    headers: Array<HTMLElement>,
    table_body: HTMLElement,
    
    label_page_info: HTMLElement,
  };

  private cached_rows: Map<Id, HTMLElement> = new Map();
  private sort_array: Array<{col: number, sort: SortFunction<T>}> = [];
  private fallbackSort: SortFunction<T>;

  private objects: Array<T> = [];

  constructor(config: Config<Id, T>, placement?: PlacementOptions) {
    this.div = createElement('div', 'simple-table', placement);

    this.fallbackSort = config.fallbackSort;
    this.rowFormatter = config.rowFormatter ?? (() => {});
    
    this.columns = config.columns.map(standardizeColumnConfig);

    let table = createElement('div', 'table', this.div);
    let table_header = createElement('div', 'header', table);
    let table_body = createElement('div', 'body', table);
    let footer = createElement('div', 'footer', this.div);

    this.elements = {
      headers: this.columns.map((col, index) => {
        let div = createElement('div', col.sort !== undefined ? 'cell sortable' : 'cell', table_header);

        div.setAttribute('_name', col.name);

        createElement('div', 'title', div).innerText = col.title;

        if (col.sort !== undefined) {
          createElement('div', 'sortIndicator', div);
          div.addEventListener('click', () => {
            let direction = div.getAttribute('sort_direction');
            if (direction === "forward") this.setSortDirection(index, "reverse");
            else if (direction === "reverse") this.setSortDirection(index, undefined);
            else this.setSortDirection(index, "forward");
            this.render();
          });
        }
        return div;
      }),
      table_body,
      label_page_info: createElement('div', 'label_page_info', footer),

    };

    new ResizeObserver(() => this.determineLayout()).observe(this.div);
  }

  private setSortDirection(col: number, direction?: "forward" | "reverse") {
    let index = this.sort_array.findIndex(x => x.col === col);

    if (index !== -1) {
      this.sort_array.splice(index, 1);
    }

    if (direction !== undefined) {
      this.elements.headers[col].setAttribute("sort_direction", direction);
      this.sort_array.unshift({
        col,
        sort: this.columns[col].sort![direction]
      });
    }else{
      this.elements.headers[col].removeAttribute("sort_direction");
    }
    this.performSort();
  }

  clear() {
    this.cached_rows.clear();
    this.setObjects([]);
  }

  setObjects(objects: Array<T>) {
    this.objects = objects;

    this.performSort();
    this.render();
  }

  clearSort() {
    this.sort_array = [];
    for (let head of this.elements.headers) {
      head.removeAttribute("sort_direction");
    }
    this.performSort();
    this.render();
  }

  private performSort() {
    this.objects.sort((a, b) => {
      for (let {sort} of this.sort_array) {
        let res = sort(a,b);
        if (res !== 0) return res;
      }
      return this.fallbackSort(a,b);
    });
  }

  private render() {
    this.renderPage();
  }

  private renderPage() {
    let table_body = this.elements.table_body;

    removeChildren(table_body);

    let items = this.objects;

    if (items.length === 0) {
      let message = createElement('div', 'empty_message');
      message.innerText = 'No results';
      table_body.append(message);
    }else{
      for (let obj of items) {
        table_body.append(this.getObjectRow(obj));
      }

      this.elements.label_page_info.innerText = `Found ${items.length} results`;
    }
    table_body.parentElement?.scrollTo({
      top: 0,
      behavior: "smooth"
    });
    // table_body.parentElement?.scrollTo({
    //   top: table_body.offsetTop,
    //   behavior: "smooth"
    // });
    // table_body.scrollIntoView({behavior: "smooth", block: "start"})

  }

  private getObjectRow(object: T): HTMLElement {
    let id = object.id;
    if (this.cached_rows.has(id)) {
      return this.cached_rows.get(id)!;
    }

    let row = createElement('div', 'row');
    this.cached_rows.set(id, row);

    for (let col of this.columns) {
      let div = col.cellFactory(object);
      div.classList.add('cell');
      div.setAttribute('_name', col.name);
      row.appendChild(div);
    }

    this.rowFormatter(object, row);

    return row;
  }

  private determineLayout() {
    let elements = [
      this.elements.label_page_info,
    ];

    if (elements.map(x => x.offsetWidth).reduce((a,b) => a +b) >= this.div.clientWidth * 0.95) {
      this.div.classList.add('compact');
    }else{
      this.div.classList.remove('compact');
    }
  }
}
