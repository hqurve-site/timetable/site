import { createElement, PlacementOptions, removeChildren } from '../../util';
import './index.scss';

export type Config<Id> = {
  formatter(id: Id): string,
  callback(id: Id): void,
}

export class ButtonSelect<Id extends number | string> {
  div: HTMLElement;

  private config: Config<Id>;

  private selectedItems: Set<Id> = new Set();

  private itemLookup: Map<Id, {
    index: number,
    element?: HTMLElement
  }> = new Map();
  private items: Array<Id> = [];

  constructor(config: Config<Id>, placement?: PlacementOptions) {
    this.div = createElement('div', 'button-select', placement);

    this.config = config;
  }

  clear() {
    this.itemLookup = new Map();
    this.selectedItems = new Set();
    this.items = [];
    removeChildren(this.div);
  }

  setItems(items: Id[]) {
    this.items = items;
    for (let i=0; i < items.length; i++) {
      let id = items[i];
      let entry = this.itemLookup.get(id);
      if (entry === undefined) {
        entry ={
          index: i,
        };
        this.itemLookup.set(id, entry);
      }else{
        entry.index = i;
      }
    }
  }

  clearSelection(){
    this.setSelectedItems([]);
    for (let id of this.selectedItems) {
      this.itemLookup.get(id)?.element?.classList.remove('selected');
    }
    this.selectedItems = new Set();
  }
  setSelectedItems(items: Iterable<Id> = []) {
    let set = new Set(items);
    
    for (let id of [...this.selectedItems]) {
      if (set.has(id)) continue;
      this.removeSelection(id);
    }
    for (let id of set) {
      this.addSelection(id);
    }
  }
  getSelectedItems(): Id[] {
    return [... this.selectedItems];
  }
  // getUnselectedItems(): Id[] {
  //   return this.items.filter(id => !this.isSelected(id));
  // }
  addSelection(id: Id) {
    this.selectedItems.add(id);
    this.itemLookup.get(id)?.element?.classList?.add?.('selected');
  }
  removeSelection(id: Id) {
    this.selectedItems.delete(id);
    this.itemLookup.get(id)?.element?.classList?.remove?.('selected');
  }
  isSelected(id: Id): boolean {
    return this.selectedItems.has(id);
  }


  renderFocused(focus: Id, width: number, showEllipses: boolean) {
    let min, max;
    if (this.items.length < width) {
      min = 0;
      max = this.items.length;
    }else{
      let index = this.itemLookup.get(focus)!.index;

      // sum to width-1
      let num_before = Math.floor((width - 1) / 2);
      let num_after = width - 1 - num_before;

      // difference of width
      let ideal_min = index - num_before; // inclusive
      let ideal_max = index + num_after + 1; // exclusive
      
      if (ideal_min < 0) {
        min = 0;
        max = width;
      }else if(ideal_max > this.items.length) {
        min = this.items.length - width;
        max = this.items.length;
      }else{
        min = ideal_min;
        max = ideal_max;
      }
    }

    let ellipseStart, ellipseEnd;
    if (showEllipses) {
      if (min <= 1) {
        ellipseStart = false;
        min = 0;
      }else{
        ellipseStart = true;
        min += 1;
      }

      if (max >= this.items.length - 1) {
        ellipseEnd = false;
        max = this.items.length;
      }else{
        ellipseEnd = true;
        max -= 1;
      }
    }else{
      ellipseStart = ellipseEnd = false;
    }

    removeChildren(this.div);
    if (ellipseStart) {
      this.div.append(
        this.getCachedElement(this.items[0]),
        createElement('div', 'ellipse'),
      );
    }
    this.div.append(...this.items.slice(min, max).map(id => this.getCachedElement(id)));
    if (ellipseEnd) {
      this.div.append(
        createElement('div', 'ellipse'),
        this.getCachedElement(this.items[this.items.length - 1]),
      );
    }
  }
  renderAll() {
    removeChildren(this.div);

    this.div.append(...this.items.map(id => this.getCachedElement(id)));
  }

  getCachedElement(id: Id): HTMLElement {
    let entry = this.itemLookup.get(id)!;
    if (entry.element === undefined) {
      entry.element = createElement('div', 'item button');
      entry.element.innerText = this.config.formatter(id);

      entry.element.addEventListener('click', () => {
        this.config.callback(id);
      });

      if (this.selectedItems.has(id)) entry.element.classList.add('selected');
    }
    return entry.element;
  }
}

export class SingleButtonSelect<Id extends number | string> {
  get div() {
    return this.inner.div;
  }
  
  private inner: ButtonSelect<Id>;
  private width?: number;
  private ellipse?: boolean;
  constructor(config: Config<Id> & { width?: number, showEllipses?: boolean}, placement?: PlacementOptions) {
    this.inner = new ButtonSelect(config, placement);
    this.width = config.width;
    this.ellipse = config.showEllipses;
  }

  setItems(items: Id[], selected?: Id) {
    this.inner.setItems(items);

    if (selected !== undefined) {
      this.setSelected(selected);
    }
  }
  getSelected(): Id {
    return this.inner.getSelectedItems()[0];
  }
  setSelected(id: Id) {
    this.inner.removeSelection(this.getSelected());
    this.inner.addSelection(id);
  }

  render() {
    if (this.width === undefined) {
      this.inner.renderAll();
    }else{
      this.inner.renderFocused(this.getSelected(), this.width, this.ellipse ?? false);
    }
  }
}
