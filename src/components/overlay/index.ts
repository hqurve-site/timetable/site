import * as util from '../../util';
import './index.scss';

type Id = number;

type MessageOptions = {
  priority?: number,
  message?: string | null | HTMLElement,
  buttons?: string[],
  closable?: boolean,
  timeout?: number, // seconds
};

type MessageCallback = (msg: string | null) => void;

type MessageEntry = {
  id: Id,
  priority: number,
  timeout: number,
  closable: boolean,
  callback: MessageCallback,
  div: HTMLElement,
};

export type MessageHandle = {
  id: Id,
  remove(): void,
  promise: Promise<string|null>,
}

function createDiv(
  type: string,
  title: string,
  options: Required<MessageOptions>,
  callback: MessageCallback,
): HTMLElement {
  // create div
  let div = util.createElement('div', `container ${type}`);
  util.createElement('div', 'icon', div);
  util.createElement('div', 'title', div).innerText = title;
  if (options.closable) {
    util.createElement('div', 'close', div).addEventListener('click', () => callback(null));
  }

  if (options.message !== null) {
    if (typeof options.message === 'string') {
      util.createElement('div', 'message default', div).innerText = options.message;
    }else{
      div.append(options.message);
      options.message.classList.add('message');
    }
  }
  if (options.buttons.length !== 0) {
    let holder = util.createElement('div', 'buttonHolder', div);
    for (let label of options.buttons) {
      let button = util.createElement('div', 'button', holder);
      button.innerText = label;
      button.addEventListener('click', () => callback(label));
    }
  }
  return div;
}
export default class Overlay {
  private idCounter = 1;
  private currentShown: undefined | number = undefined;

  div: HTMLElement;
  private holder: HTMLElement;

  private stack: MessageEntry[] = [];

  constructor(placement?: util.PlacementOptions) {
    this.div = util.createElement('div', 'overlay messageOverlay', placement);
    this.holder = this.div;

    this.div.addEventListener('click', event => {
      if (this.currentShown !== undefined && event.target === this.div) {
        this.resolve(this.currentShown, null);
      }
    });
  }

  // returns an object with the following
  //   - id
  //   - promise which resolves to either null or 
  addMessage(type: string, title: string, options: MessageOptions= {}): MessageHandle {
    let self = this;
    let id = this.idCounter++;
    let completeOptions = Object.assign({
      priority: 0,
      message: null,
      buttons: [],
      closable: true,
      timeout: Infinity, // seconds
    }, options);

    let entry: Partial<MessageEntry> = {
      id,
      priority: completeOptions.priority,
      timeout: completeOptions.timeout,
      closable: completeOptions.closable,
    };
    let promise = new Promise<string | null>((resolve) => { entry.callback = resolve; });

    entry.div = createDiv(type, title, completeOptions, msg => self.resolve(id, msg));

    let index = this.stack.findIndex(v => v.priority < completeOptions.priority);

    if (index === -1){
      this.stack.push(entry as MessageEntry);
    }else{
      this.stack.splice(index, 0, entry as MessageEntry);
    }

    this.update();

    return {
      id, promise,
      remove() {
        self.remove(id);
      }
    };
  }
  clear() {
    let stack = this.stack;
    this.stack = [];
    for (let entry of stack){
      entry.callback(null);
    }
  }
  private remove(id: Id) {
    let index = this.stack.findIndex(v => v.id === id);
    if (index === -1) return false;
    this.stack.splice(index, 1);
    this.update();
    return true;
  }

  private resolve(id: Id, response: string | null) {
    if (id !== this.currentShown) return;
    if (response === null && !this.stack[0].closable) return;
    let entry = this.stack.shift() as MessageEntry;
    entry.callback(response);
    this.update();
  }

  private update() {
    if (this.stack[0]?.id === this.currentShown) return;

    util.removeChildren(this.holder);
    this.currentShown = undefined;

    if (this.stack.length === 0) {
      this.div.removeAttribute('visible');
      return;
    }
    this.div.setAttribute('visible', 'true');

    this.currentShown = this.stack[0].id;
    this.holder.appendChild(this.stack[0].div);

    if (this.stack[0].timeout < Infinity) {
      let id = this.currentShown;
      let self = this;
      setTimeout(() => self.resolve(id, null), this.stack[0].timeout * 1000);
    }
  }
}
