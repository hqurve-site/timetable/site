import './index.scss';
import { createElement, PlacementOptions, removeChildren, removeFromParent } from "../../util";


export type Config<Id, T=never> = {
  empty_message?: string,
  drag_identifier?: string,
  createElement(id: Id): {
    div: HTMLElement,
    handle?: HTMLElement,
    dropzone?: HTMLElement,
    extra_data: T,
  },
  serializeId(id: Id): string,
  deserializeId(id: string): Id,

  reorder_callback?: (ids: Array<Id>, source?: 'userInput') => void,
}

type Separator = {
  div: HTMLElement,
  highlight_reasons: Set<'body' | number>
};
function createSeparator(placement?: PlacementOptions): Separator {
  let div = createElement('div', 'separator', placement);
  createElement('div', 'inner', div);
  return {
    div,
    highlight_reasons: new Set(),
  };
}

export class ReorderableList<Id, T=never> {
  div: HTMLElement;
  private config: Omit<Required<Config<Id, T>>, "empty_message">;

  private inner: {
    prev_item_order: Array<Id>,
    item_order: Array<Id>,
  };

  private elements: {
    empty_element?: {div: HTMLElement, sep: Separator},
    separators: Array<Separator>, // one longer than the length of item_order
    items: Map<Id, {div: HTMLElement, handle: HTMLElement, dropzone: HTMLElement, extra_data: T}>,
  };

  constructor(config: Config<Id, T>, placement?: PlacementOptions) {
    this.config = {
      drag_identifier: (config.drag_identifier ?? "ReorderableList").toLowerCase() + `${Math.floor(Math.random() * 65536)}`,
      createElement: config.createElement,
      serializeId: config.serializeId,
      deserializeId: config.deserializeId,
      reorder_callback: config.reorder_callback ?? (() => {}),
    };

    this.div = createElement('div', 'reorderable-list', placement);

    this.inner = {
      prev_item_order: [],
      item_order: [],
    };
    this.elements = {
      separators: [],
      items: new Map(),
    };
    if (config.empty_message !== undefined) {
      this.elements.empty_element = {
        div: createElement('div', 'empty'),
        sep: createSeparator(),
      };
      this.elements.empty_element.div.innerText = config.empty_message;
    }

    this.div.addEventListener('dragover', event => {
      if (event.target === this.div) {
        this.handleDrag('dragover', {type: 'body'}, event);
      }else{
        this.handleDrag('dragleave', {type: 'body'}, event);
      }
    });
    this.div.addEventListener('dragleave', event => {
      this.handleDrag('dragleave', {type: 'body'}, event);
    });
    this.div.addEventListener('drop', event => {
      if (event.target === this.div){
        this.handleDrag('drop', {type: 'body'}, event);
      }
    });

    this.setItems([]);
  }

  private triggerCallback(source?: 'userInput') {
    if (this.inner.prev_item_order.length === this.inner.item_order.length
      && this.inner.prev_item_order.every((id, i) => this.inner.item_order[i] === id)
    ) return;

    this.inner.prev_item_order = [...this.inner.item_order];
    this.config.reorder_callback([...this.inner.item_order], source);
  }

  // remove from lists but keep cached info
  // This method would cleanup the cached info and throw errors if there are residues elsewhere
  private cleanup(ids: Iterable<Id>) {
    for (let id of ids) {
      if (this.inner.item_order.includes(id)) throw `Item ${id} was not removed from inner.item_order`;

      let item = this.elements.items.get(id);
      if (item === undefined) throw `Item ${id} was incorreclty removed from elements.items`;
      if (item.div.parentElement === this.div) throw `Item ${id} was not removed from div`;
      this.elements.items.delete(id);
    }
    if (this.elements.separators.length !== this.inner.item_order.length + 1) {
      throw `Separators length (${this.elements.separators.length}) is not one more than item_order length (${this.inner.item_order.length})`;
    }
  }
  private addEmptyElement() {
    if (this.inner.item_order.length !== 0) return;
    if (this.elements.empty_element === undefined) return;
    this.removeEmptyElement();

    this.div.appendChild(this.elements.empty_element.div);
    this.div.appendChild(this.elements.empty_element.sep.div);
  }
  private removeEmptyElement() {
    if (this.elements.empty_element === undefined) return;
    removeFromParent(this.elements.empty_element.div);
    removeFromParent(this.elements.empty_element.sep.div);
  }

  clear() {
    this.setItems([]);

    this.triggerCallback();
  }
  setItems(ids: Iterable<Id>) {
    let set = new Set(ids);

    removeChildren(this.div);

    this.inner.item_order = [];
    this.elements.separators = [];
    
    this.elements.separators.push(createSeparator({parent: this.div}));
    for (let id of ids) {
      this.div.append(this.getItem(id).div);
      this.inner.item_order.push(id);
      this.elements.separators.push(createSeparator({parent: this.div}));
    }
    this.addEmptyElement();

    let ids_to_remove = this.inner.item_order.filter(id => !set.has(id));
    this.cleanup(ids_to_remove);

    this.triggerCallback();
  }
  delete(id: Id) {
    let index = this.inner.item_order.indexOf(id);
    if (index === -1) throw `List does not contain ${id}`;

    removeFromParent(this.getItem(id).div);
    this.inner.item_order.splice(index, 1);
    
    removeFromParent(this.elements.separators[index].div);
    this.elements.separators.splice(index, 1);

    this.addEmptyElement();

    this.cleanup([id]);

    this.triggerCallback();
  }
  append(id: Id) {
    if (this.inner.item_order.includes(id)) throw `List already contains ${id}`;
    this.inner.item_order.push(id);

    let item = this.getItem(id);
    this.div.append(item.div);
    this.elements.separators.push(createSeparator({parent: this.div}));

    this.removeEmptyElement();
    this.triggerCallback();
  }
  replace(old_id: Id, new_id: Id) {
    let index = this.inner.item_order.indexOf(old_id);
    if (index === -1) throw `List does not contain ${old_id}`;

    this.inner.item_order.splice(index, 1, new_id);
    let old_item = this.getItem(old_id);
    let new_item = this.getItem(new_id);
    old_item.div.replaceWith(new_item.div);

    this.cleanup([old_id]);

    this.triggerCallback();
  }

  getElementDiv(id: Id): HTMLElement | undefined {
    return this.elements.items.get(id)?.div;
  }
  getElementData(id: Id): T | undefined {
    return this.elements.items.get(id)?.extra_data;
  }

  private getItem(id: Id): {div: HTMLElement, handle: HTMLElement, dropzone: HTMLElement} {
    if (this.elements.items.has(id)) return this.elements.items.get(id)!;

    let res = this.config.createElement(id);

    let item = {
      div: res.div,
      handle: res.handle ?? res.div,
      dropzone: res.dropzone ?? res.div,
      extra_data: res.extra_data
    };
    item.div.classList.add('item');
    this.elements.items.set(id, item);
    this.setupDragAndDrop(id, item);

    return item;
  }

  private setupDragAndDrop(id: Id, item: {div: HTMLElement, handle: HTMLElement, dropzone: HTMLElement}) {
    let {div, handle, dropzone} = item;
    

    // setup drag
    handle.draggable = true;
    handle.addEventListener('dragstart', event => {
      if (event.dataTransfer === null) return;
      event.dataTransfer.setData(this.config.drag_identifier, this.config.serializeId(id));

      div.classList.add('dragging');
    });
    handle.addEventListener('dragend', () => {
      div.classList.remove('dragging');
    });

    // setup drop
    dropzone.addEventListener('dragover', event => {
      this.handleDrag('dragover', {type: 'item', id}, event);
    });
    dropzone.addEventListener('dragleave', event => {
      this.handleDrag('dragleave', {type: 'item', id}, event);
    });
    dropzone.addEventListener('drop', event => {
      this.handleDrag('drop', {type: 'item', id}, event);
    });
  }

  private handleDrag(type: 'dragover' | 'dragleave' | 'drop', ele: {type: 'item', id: Id} | {type: 'body'}, event: DragEvent) {
    if (event.dataTransfer === null || !event.dataTransfer.types.includes(this.config.drag_identifier)) return;

    let possibleDropIndices;
    let dropIndex;
    let highlight_reason: 'body' | number;
    if (ele.type === 'body') {
      highlight_reason = 'body';
      dropIndex = this.elements.separators.length - 1;
      possibleDropIndices = [ dropIndex ];
    }else{
      let index = this.inner.item_order.indexOf(ele.id);
      highlight_reason = index;
      possibleDropIndices = [index, index + 1];

      let dropzone = this.getItem(ele.id).dropzone;
      if (event.clientY - dropzone.getBoundingClientRect().top < dropzone.clientHeight / 2) {
        dropIndex = index;
      }else{
        dropIndex = index + 1;
      }
    }

    for (let index of possibleDropIndices) {
      this.elements.separators[index].highlight_reasons.delete(highlight_reason);
    }
    if (type === 'dragover') {
      event.preventDefault();
      this.elements.separators[dropIndex].highlight_reasons.add(highlight_reason);
    }else if(type === 'dragleave'){
    }else if(type === 'drop'){
      let id = this.config.deserializeId(event.dataTransfer.getData(this.config.drag_identifier));
      let oldIndex = this.inner.item_order.indexOf(id);

      this.performDrop(oldIndex, dropIndex);
    }


    for (let index of possibleDropIndices) {
      let sep = this.elements.separators[index];
      if (sep.highlight_reasons.size === 0) {
        sep.div.classList.remove('visible');
      }else{
        sep.div.classList.add('visible');
      }
    }
  }

  // | 1 | A | 2 | 3 | 4 | 5 |
  //     ^oldIndex       ^dropIndex 
  // we want
  // | 1 | 2 | 3 | 4 | A | 5 |
  //     ^oldIndex       ^dropIndex 
  //
  // OR
  //
  // | 1 | 2 | 3 | 4 | A | 5 |
  //     ^dropIndex  ^oldIndex 
  // we want
  // | 1 | A | 2 | 3 | 4 | 5 |
  //     ^dropIndex  ^oldIndex 
  //
  private performDrop(oldIndex: number, dropIndex: number) {
    if (dropIndex === oldIndex || dropIndex == oldIndex + 1) return;
    let id = this.inner.item_order[oldIndex];
    let div = this.getElementDiv(id)!;

    removeFromParent(div); // this causes the separators at oldIndex and oldIndex+1 to be right next to each other

    this.elements.separators[dropIndex].div.before(div);

    if (dropIndex < oldIndex) { // remove then insert
      this.inner.item_order.splice(oldIndex, 1);

      this.inner.item_order.splice(dropIndex, 0, id);

      // We have a situation like
      //  | X A | X | X .... | X | X |   |
      //        ^dropIndex           ^oldIndex
      for (let i=dropIndex; i<= oldIndex; i++){
        let sep = this.elements.separators[i];

        let siblingBefore = sep.div.previousElementSibling!;
        removeFromParent(sep.div);
        siblingBefore.before(sep.div);
      }
    }else if(dropIndex > oldIndex) { // insert then remove
      this.inner.item_order.splice(dropIndex, 0, id);

      this.inner.item_order.splice(oldIndex, 1);


      // We have a situation like
      //  | X | | X | X .... | X | X A | X |
      //      ^oldIndex                ^dropIndex
      for (let i=oldIndex+1; i<= dropIndex - 1; i++){
        let sep = this.elements.separators[i];

        let siblingAfter = sep.div.nextElementSibling!;
        removeFromParent(sep.div);
        siblingAfter.after(sep.div);
      }
    }

    this.triggerCallback('userInput');
  }
}
