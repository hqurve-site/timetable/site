import { PROFILE_LIST, ProfileKey } from '../../globals';
import { createElement, PlacementOptions, removeChildren } from '../../util';
import './index.scss'



export class ProfilePickerOverlay {
    div: HTMLElement;

    private selected_semester?: string;
    private elements: {
        semester_list_holder: HTMLElement,
        semester_list: Array<{ // sorted newest to oldest
            semester: string,
            dates: Array<Date>, // sorted newest to oldest
            element: HTMLElement
        }>,

        btn_latest_in_profile: HTMLElement,
        profile_list_holder: HTMLElement,
    };
    private callback?: (profile_key?: ProfileKey) => void;


    constructor(placement?: PlacementOptions) {
        this.div = createElement('div', 'profile-picker-overlay', placement);
        this.div.addEventListener('click', event => {
            if (event.target === this.div) {
              this.perform_callback();
            }
        });

        let container = createElement('div', 'holder', this.div);

        let header = createElement('div', 'header', container);
        let table = createElement('div', 'table', container);
        let footer = createElement('div', 'footer', container);

        // populate header
        let title = createElement('div', 'title', header);
        title.textContent = "Choose version of timetable"
        createElement('div', 'close', header).addEventListener('click', () => this.perform_callback())

        // populate footer
        let btn_latest = createElement('div', 'button', footer);
        btn_latest.innerText = "latest";
        btn_latest.addEventListener('click', () => this.perform_callback({}))

        // populate body
        let sidebar = createElement('div', 'sidebar', table);
        let content = createElement('div', 'content', table);

        let semester_list_holder = createElement('div', 'list', sidebar);
        let btn_latest_in_profile = createElement('div', 'button latest', content);
        let profile_list_holder = createElement('div', 'list', content)

        btn_latest_in_profile.textContent = "latest in semester";
        btn_latest_in_profile.addEventListener('click', () => this.perform_callback({
            semester: this.selected_semester
        }));
        
        // get set of semesters and their profiles
        let semester_dates: {[semester:string]: Array<Date>} = {};
        for (let profile of PROFILE_LIST) {
            semester_dates[profile.semester] = semester_dates[profile.semester] ?? [];
            semester_dates[profile.semester].push(profile.publish_date);
        }
        let semester_list = [];
        for (let [semester, dates] of
            // sort newest to oldest
            Object.entries(semester_dates).sort((a,b) => b[0].localeCompare(a[0]))
        ) {
            let element = createElement('div', 'semester', semester_list_holder);
            let title = createElement('div', 'title', element);
            title.textContent = semester;

            createElement('div', 'selected_indicator', element);

            semester_list.push({
                semester: semester,
                dates: dates.sort((a,b) => a < b ? -1 : a > b ? 1 : 0).reverse(), // sort newest to oldest
                element: element,
            })

            element.addEventListener('click', () => this.show_semester(semester))
        }
        console.log(semester_list)

        this.elements = {
            semester_list_holder,
            semester_list,
            btn_latest_in_profile,
            profile_list_holder
        }  
    }

    hide() {
        this.div.classList.remove('shown');
    }
    show(profile_key?: ProfileKey, callback?: (profile_key?: ProfileKey) => void) {
        this.callback = callback;
        this.show_semester(profile_key?.semester, profile_key?.at);
        this.div.classList.add('shown');
    }

    private perform_callback(profile_key?: ProfileKey) {
        this.hide();
        this.callback?.(profile_key);
    }

    private show_semester(semester?: string, at?: string) {
        // clear the selected indicators
        for (let {element} of this.elements.semester_list) {
            element.classList.remove("selected");
        }
        // clear content
        removeChildren(this.elements.profile_list_holder);

        // select the matching. If there is none, we just choose the most recent
        let semester_obj =
            this.elements.semester_list.find(obj => obj.semester === semester)
            ?? this.elements.semester_list[0];
        semester_obj.element.classList.add('selected');

        // activate latest btn
        this.selected_semester = semester_obj.semester;

        // add the children
        for (let date of semester_obj.dates) {
            let div = createElement('div', 'profile', this.elements.profile_list_holder);
            div.textContent = date.toLocaleString(undefined, {
                weekday: "long",
                year: "numeric",
                month: "long",
                day: "2-digit",
                hour: "2-digit",
                minute: "2-digit",
                second: "2-digit"
            });
            div.addEventListener('click', () => this.perform_callback({
                semester: semester_obj.semester,
                at: date.toISOString(),
            }))
        }
    }
}