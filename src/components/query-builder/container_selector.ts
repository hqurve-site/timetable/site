import { Builder, Callbacks, ExtraData, GenQueryResponse } from ".";
import { Container, ContainerId } from "../../data";
import { ContainerQuery } from "../../data/api";
import { ContainerReference } from "../../data/reference/container";
import { addOnFinishedTypingListener, createElement, generateContainerTitle } from "../../util";
import { SimpleTable } from "../tables";
import { ButtonSelect } from "../button-select";
import Overlay from "../overlay";

import './container_selector.scss';

export type Data = null;
export type SerializableData = null;

export function deserialize(data: SerializableData): Data {
  return data;
}
export function serialize(data: Data) : SerializableData {
  return data;
}

export async function generateQuery(_data: Data, _extra_data: ExtraData): Promise<GenQueryResponse> {
  return {
    type: "error",
    error: "No container selected",
  };
}

export function createUI(_data: Data, extra_data: ExtraData, callbacks: Callbacks): HTMLElement {
  let div = createElement('div', 'query-builder container-selector');
  let profile = extra_data.profile;

  let elements = {
    txt_search: createElement('input', 'txt_filter', div),
    btn_search: createElement('div', 'search_button button', div),
    type_filter: new ButtonSelect<string>({
      formatter: i => i,
      callback: i => {
        if (elements.type_filter.isSelected(i)) elements.type_filter.removeSelection(i);
        else elements.type_filter.addSelection(i);
        update();
      },
    }, {
      parent: div,
      extraClasses: ['type_filter'],
    }),
    display: new SimpleTable<ContainerId, Container>({
      columns: [
        { name: 'type',
          title: 'Type',
          property: 'type',
          sort: (a,b) => a.type.toLowerCase().localeCompare(b.type.toLowerCase()),
        },
        { name: 'label',
          title: 'Code / Name',
          getter: a => generateContainerTitle(a),
          sort: (a,b) => a.code.toLowerCase().localeCompare(b.code.toLowerCase()),
        },
        { name: 'nosessions',
          title: '# Sessions',
          getter: a => {
            if (a.sessionIds.length === 1) return '1 Session';
            else return a.sessionIds.length + " Sessions";
          },
          sort: (a,b) => a.sessionIds.length - b.sessionIds.length,
        },
      ],
      fallbackSort: (a,b) => a.id - b.id,
      rowFormatter: (a, element) => {
        element.addEventListener('click', async () => {
          let key = await profile.dataAPI.create_container_key(a.id);
          let msg = elements.overlay.addMessage('loading', 'Loading Container', {closable: false});
          let container_reference = await ContainerReference.new(key!, profile);

          callbacks.replaceWith('timetable_session_chooser', Builder.deserializeData('timetable_session_chooser', {container: container_reference!}));
          msg.remove();
        });
      },
    }, {
      parent: div,
      extraClasses: [ 'table' ],
    }),
    overlay: new Overlay({parent: div})
  };

  elements.txt_search.placeholder = "Search";
  addOnFinishedTypingListener(elements.txt_search, () => update());

  elements.type_filter.setItems(profile.container_types);
  elements.type_filter.setSelectedItems(profile.container_types);
  elements.type_filter.renderAll();

  elements.btn_search.addEventListener('click', () => update());


  let load_in_progress = false;
  async function update() {
    if (load_in_progress) return;
    load_in_progress = true;
    let search_message = elements.overlay.addMessage('loading', 'Performing search', {closable: false});
    try {
      let ids = await performSearch();
      search_message.remove();

      let containers = [];
      for (let id of ids) {
        let container = profile.dataAPI.get_loaded_container(id);
        if (container !== undefined) containers.push(container);
      }
      elements.display.setObjects(containers);
    }catch(e) {
      search_message.remove();
      console.error(e);
      search_message.remove();
      await elements.overlay.addMessage('error', 'Search error', {message: "" + e}).promise;
    }

    load_in_progress = false;
  }

  async function performSearch(): Promise<ContainerId[]> {
    let shown_types = elements.type_filter.getSelectedItems();
    let search_input = elements.txt_search.value;

    let query: ContainerQuery = { type: 'search', string: search_input };
    if (shown_types.length !== 0) {
      query = { type: 'container-check', inner: query, checks: shown_types.map(type => ({type})) };
    }

    let ids = await profile.dataAPI.query_containers(
      query,
      {load_objects: true},
    );

    return ids;
  }

  update();

  return div;
}
