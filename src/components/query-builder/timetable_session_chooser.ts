import { Callbacks, GenQueryResponse, ExtraData } from ".";
import { Container, SessionId } from "../../data";
import { SessionQuery } from "../../data/api";
import { SessionKey, sessionKeyEquals, stringifyContainerKey } from "../../data/keys";
import { ContainerReference, SeriableContainerReference } from "../../data/reference/container";
import { Profile } from "../../globals";
import { createElement, generateContainerTitle, KeyedPromise } from "../../util";
import Overlay from "../overlay";
import TimetableView from "../timetable";

import './timetable_session_chooser.scss';

export type Data = {
  container: ContainerReference,
  hidden_sessions: Set<SessionKey>,

  promise: KeyedPromise<Profile["id"], LoadResponse>,
};
export type SerializableData = 
  { container: ContainerReference }
| { version: "1",
    container: SeriableContainerReference,
    hidden_sessions: Array<SessionKey>,
  }

export function deserialize(data: SerializableData): Data {
  if (!('version' in data)) {
    return {
      container: data.container,
      hidden_sessions: new Set(),

      promise: new KeyedPromise(),
    };

  }else if (data.version === "1") {
    return {
      container: ContainerReference.deserialize(data.container),
      hidden_sessions: new Set(data.hidden_sessions),

      promise: new KeyedPromise(),
    };
  }else{
    throw "how did we get here?";
  }
}
export function serialize(data: Data) : SerializableData {
  return {
    version: "1",
    container: data.container.serialize(),
    hidden_sessions: [...data.hidden_sessions],
  };
}

type LoadResponse =
  { status: 'found',
    
    container: Container,
    session_lookup: Map<SessionId, SessionKey>,
    hidden_sessions: Set<SessionKey>,

    addedSessions: Array<SessionKey>,
    removedSessions: Array<SessionKey>,

    trigger_change(): void,
  }
| { status: 'missing' };
function load(data: Data, extra_data: ExtraData): Promise<LoadResponse> {
  let profile = extra_data.profile;
  return data.promise.run(profile.id, async (trigger_change) => {
    let res = await data.container.resolve(profile);
    if (res.status === 'missing') return {status: 'missing'};

    let {container, addedSessions, removedSessions} = res;

    let session_lookup = new Map(res.sessions.map(({id, key}) => [id, key]));

    let old = [...data.hidden_sessions];
    let hidden_sessions = new Set(res.sessions.map(({key}) => key).filter(key => old.some(x => sessionKeyEquals(key, x))));

    let _res = res;
    return {
      status: 'found',
      container,
      session_lookup,
      hidden_sessions,
      addedSessions,
      removedSessions,
      trigger_change(){_res.trigger_change(); trigger_change();}
    };
  }, res => {
    if (res.status === 'missing') return {status: 'missing'};
    data.hidden_sessions = res.hidden_sessions;
    return {
      status: 'found',
      container: res.container,
      session_lookup: res.session_lookup,
      hidden_sessions: res.hidden_sessions,
      addedSessions: [],
      removedSessions: [],
      trigger_change(){}
    };
  });
}

export async function generateQuery(data: Data, extra_data: ExtraData): Promise<GenQueryResponse> {
  let res = await load(data, extra_data);

  if (res.status === 'found') {
    let {hidden_sessions, addedSessions, removedSessions} = res;

    let warning: string | undefined;
    if (addedSessions.length !== 0){
      if (removedSessions.length !== 0) {
        warning = `${addedSessions.length} sessions added and ${removedSessions.length} sessions removed`;
      }else{
        warning = `${addedSessions.length} sessions added`;
      }
    }else{
      if (removedSessions.length !== 0) {
        warning = `${removedSessions.length} sessions removed`;
      }else{
        warning = undefined;
      }
    }

    let query: SessionQuery = {
      type: 'session-check',
      inner: {
        type: 'container-key-pool',
        key: data.container.key,
      },
      checks: [...hidden_sessions],
      negate: true,
    };

    if (warning !== undefined) {
      return {
        type: 'warning',
        warning,
        query,
      };
    }else{
      return {
        type: 'query',
        query,
      };
    }
  }else {
    return {
      type: 'error',
      error: `unable to load container ${stringifyContainerKey(data.container.key)}`
    };
  }
}

export function createUI(data: Data, extra_data: ExtraData, callbacks: Callbacks): HTMLElement {
  let div = createElement('div', 'query-builder timetable-session-chooser');

  let session_lookup = new Map<SessionId, SessionKey>()
  let hidden_sessions: Set<SessionKey> = new Set();

  let profile = extra_data.profile;

  let elements = {
    title: createElement('div', 'title', div),
    btn_select_all: createElement('div', 'button select-all', div),
    btn_select_none: createElement('div', 'button select-none', div),
    timetable: new TimetableView({
      sessionGetter: id => profile.dataAPI.get_loaded_session(id),
      hours: profile.timetable_hours,
      days: profile.timetable_days,
      cellConfig: {
        formatter: (id, cell) => {
          let key = session_lookup.get(id)!;
          cell.classList.add('cell');
          cell.addEventListener('click', () => {
            if (hidden_sessions.has(key)) { // hidden
              hidden_sessions.delete(key);
              cell.classList.add('selected');
            }else{
              hidden_sessions.add(key);
              cell.classList.remove('selected');
            }
            callbacks.queryChanged();
          })
          if (!hidden_sessions.has(key)) {
            cell.classList.add('selected');
          }
        },
        dateGenerator: (week, day) => profile.calculate_date(week, day),
        container_type_order: () => profile.container_types,
      },
    }, {
      parent: div,
      extraClasses: ['timetable'],
    }),
    overlay: new Overlay({parent: div, extraClasses: ['overlay']}),
  };

  // setup the select all and select none buttons
  elements.btn_select_all.innerText = "Select all";
  elements.btn_select_none.innerText = "Select none";
  elements.btn_select_all.addEventListener('click', () => {
    hidden_sessions.clear();
    for (let id of session_lookup.keys()) {
      elements.timetable.getCell(id)?.classList?.add('selected')
    }
    callbacks.queryChanged();
  });
  elements.btn_select_none.addEventListener('click', () => {
    // note that we do not simply overwrite the values of hidden_sessions
    // since it is actually a reference to data
    for (let [id, key] of session_lookup.entries()) {
      hidden_sessions.add(key);
      elements.timetable.getCell(id)?.classList?.remove('selected')
    }
    callbacks.queryChanged();
  });

  let overlay = elements.overlay;

  let loading_message = overlay.addMessage('loading', 'Loading Container', {closable: false});
  load(data, extra_data).then(async res => {
    loading_message.remove();
    if (res.status === 'found') {
      let {container, session_lookup: sessions, addedSessions, removedSessions} = res;

      let warning_message;
      if (addedSessions.length !== 0 || removedSessions.length !== 0) {
        let message = createElement('div', 'warning sessions_changed');
        if (addedSessions.length !== 0) {
          let div = createElement('div', undefined, message);

          createElement('div', 'header', div).innerText = 'Added Sessions:';
          let body = createElement('div', 'body', div);
          for (let session of addedSessions) {
            createElement('div', 'item', body).innerText = JSON.stringify(session);
          }
        }
        if (removedSessions.length !== 0) {
          let div = createElement('div', undefined, message);

          createElement('div', 'header', div).innerText = 'Removed Sessions:';
          let body = createElement('div', 'body', div);
          for (let session of removedSessions) {
            createElement('div', 'item', body).innerText = JSON.stringify(session);
          }
        }
        warning_message = overlay.addMessage('warning', 'Sessions Changed', {message});
      }else{
        warning_message === undefined;
      }

      session_lookup = res.session_lookup;
      hidden_sessions = res.hidden_sessions;
      
      elements.title.innerText = generateContainerTitle(container);
      callbacks.setDefaultName(container.code);
      elements.timetable.setSessions([...sessions.keys()]);
      elements.timetable.update();

      if (warning_message !== undefined) {
        await warning_message.promise;
        res.trigger_change();
        callbacks.queryChanged();
      }else{
        res.trigger_change();
      }
    }else{
      let message = createElement('div', 'error no_container');

      createElement('div', 'header', message).innerText = "Unable to find any containers with the following properties:";

      let body = createElement('div', 'body', message);
      createElement('div', 'item', body).innerText = `type: ${data.container.key.type}`;
      createElement('div', 'item', body).innerText = `code: ${data.container.key.code}`;

      overlay.addMessage('error', 'Unable to fetch Container', {closable: false, message});
    }
  }).catch(e => {
    overlay.clear();
    overlay.addMessage('error', 'Unexpected Error', {closable: false, message: JSON.stringify(e) });
    console.error('Unexpected Error', e);
  });
  return div;
}
