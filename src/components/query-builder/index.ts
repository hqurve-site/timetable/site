
import { Profile } from '../../globals';
import * as container_selector from './container_selector';
import * as timetable_session_chooser from './timetable_session_chooser';
import { SessionQuery } from '../../data/api';
import { KeyedPromise } from '../../util';



// placed at the top so that its easier to spot
export type BuilderTypes = {
  container_selector: {
    data: container_selector.Data,
    serdeData: container_selector.SerializableData,
  },
  timetable_session_chooser: {
    data: timetable_session_chooser.Data,
    serdeData: timetable_session_chooser.SerializableData,
  },

};


const handlers: {
  [K in keyof BuilderTypes]: Handler<K>
}
= {
  container_selector: {
    deserialize: container_selector.deserialize,
    serialize: container_selector.serialize,
    generateQuery: container_selector.generateQuery,
    createUI: container_selector.createUI,
  },
  timetable_session_chooser: {
    deserialize: timetable_session_chooser.deserialize,
    serialize: timetable_session_chooser.serialize,
    generateQuery: timetable_session_chooser.generateQuery,
    createUI: timetable_session_chooser.createUI,
  },
};


export type SerializableBuilder<K extends keyof BuilderTypes = keyof BuilderTypes> = {
  id: number,

  type: K,
  name?: string,
  default_name: string,
  data: BuilderTypes[K]["serdeData"],
}


export type Callbacks = {
  setDefaultName(name: string): void,
  dataChanged(): void,
  queryChanged(): void,

  replaceWith<K extends keyof BuilderTypes>(type: K, data: BuilderTypes[K]["data"]): void,
};


export type GenQueryResponse = 
  { type: "query", query: SessionQuery }
| { type: "warning", warning: string, query: SessionQuery }
| { type: "error", error: string };

export type ExtraData = {
  profile: Profile,
}
type Handler<K extends keyof BuilderTypes> = {
  deserialize(data: BuilderTypes[K]["serdeData"]): BuilderTypes[K]["data"],
  serialize(data: BuilderTypes[K]["data"]): BuilderTypes[K]["serdeData"],
  generateQuery(data: BuilderTypes[K]["data"], extra_data: ExtraData): Promise<GenQueryResponse>,
  createUI(data: BuilderTypes[K]["data"], extra_data: ExtraData, callbacks: Callbacks): HTMLElement,
}


function getHandler<K extends keyof BuilderTypes>(type: K): Handler<K> {
  if (type in handlers) {
    return handlers[type];
  }else{
    throw `invalid type '${type}'`;
  }
}

export class Builder<K extends keyof BuilderTypes=keyof BuilderTypes> {
  private inner: {
    id: number,
    type: K,
    name?: string,
    default_name: string,
    data: BuilderTypes[K]['data'],

    query_version: number,
    query: KeyedPromise<string, GenQueryResponse & {version: number}>
  };

  get id() { return this.inner.id; }
  get query_version() { return this.inner.query.counter; }
  get name() { return this.inner.name };
  set name(name) { this.inner.name = name; };
  get default_name() { return this.inner.default_name; }

  private constructor(inner: {
    id: number,
    type: K,
    name?: string,
    default_name: string,
    data: BuilderTypes[K]['data'],
  }) {
    this.inner = {
      ...inner,
      query_version: 1,
      query: new KeyedPromise(),
    };
  }

  static create<K extends keyof BuilderTypes>(id: number, type: K, data: BuilderTypes[K]["data"]): Builder<K> {
    return new Builder({
      id, type, data, default_name: `Untitled ${id}`,
    })
  }

  static deserializeData<K extends keyof BuilderTypes>(type: K, data: BuilderTypes[K]['serdeData']): BuilderTypes[K]['data'] {
    return getHandler(type).deserialize(data);
  }

  static deserialize<K extends keyof BuilderTypes>(builder: SerializableBuilder<K>): Builder<K> {
    let {id, type, name, default_name, data} = builder;
    return new Builder({
      id, type, name, default_name,
      data: Builder.deserializeData(type, data),
    })
  }
  serialize(): SerializableBuilder<K> {
    let {id, type, name, default_name, data} = this.inner;
    return {
      id, type, name, default_name,
      data: getHandler(type).serialize(data),
    }
  }

  generateQuery(extra_data: ExtraData): Promise<GenQueryResponse & {version: number}> {
    let key = `${this.inner.query_version}_${extra_data.profile.id}`;
    return this.inner.query.run(key, async (_cb, version) => {
      let {type, data} = this.inner;
      let response = await getHandler(type).generateQuery(data, extra_data);
      return {...response, version};
    });
  }
  createUI(extra_data: ExtraData, callbacks: Callbacks): HTMLElement {
    let {type, data} = this.inner;

    let _callbacks: Callbacks = {
      dataChanged: () => callbacks.dataChanged(),
      setDefaultName: name => {
        this.inner.default_name = name;
        callbacks.setDefaultName(name);
      },
      queryChanged: () => {
        this.inner.query_version++;
        callbacks.queryChanged();
      },
      replaceWith: (type, data) => callbacks.replaceWith(type, data),
    };
    return getHandler(type).createUI(data, extra_data, _callbacks);
  }
}
