import './index.scss';

import {Profile, getProfile, ProfileKey, profileKeyToString} from '../../globals';

import { Overlay, ReorderableList, TimetableView } from '../../components';

import {
  Builder as Fragment,
  SerializableBuilder as SerializableFragment,
  Callbacks as FragmentCallbacks,
  BuilderTypes as FragmentTypes,
} from '../query-builder';
import { SessionQuery } from '../../data/api';
import { addOnFinishedTypingListener, createElement, removeChildren } from '../../util';
import { generateViewerLink, ViewerParams } from '../../pages/viewer/params';
import { ProfilePickerOverlay } from '../profile-picker-overlay';

export type AgendaUICallbacks = {
  dataChanged(): void,
  queryChanged(): void,
};


export type SeriableAgenda = 
  {
    version: "1",
    id: number,
    profile_key: ProfileKey,
    title: string | undefined,

    fragments: SerializableFragment[],
    fragment_id_counter: number,
  }
;

export class Agenda {
  readonly id: number;
  profile_key: ProfileKey;
  profile: Profile;
  title: string | undefined;

  fragments: Map<number, Fragment>;
  fragment_order: number[]; // just for visuals
  fragment_id_counter: number;

  private constructor(inner: {
    id: number,
    title?: string,
    profile_key: ProfileKey,

    fragments: Array<Fragment>,
    fragment_id_counter: number,
  }) {
    this.id = inner.id;
    this.title = inner.title;
    this.profile_key = inner.profile_key;
    this.profile = getProfile(inner.profile_key);
    this.fragments = new Map(inner.fragments.map(f => [f.id, f]));
    this.fragment_order = inner.fragments.map(f => f.id);
    this.fragment_id_counter = inner.fragment_id_counter;
  }
  static create(id: number): Agenda {
    return new Agenda({
      id,
      profile_key: {},
      fragments: [],
      fragment_id_counter: 0,
    })
  }

  serialize(): SeriableAgenda {
    return {
      version: "1",
      id: this.id,
      profile_key: this.profile_key,
      title: this.title,
      fragments: this.fragment_order.map(fid => this.fragments.get(fid)!.serialize()),
      fragment_id_counter: this.fragment_id_counter,
    }
  }

  static deserialize(agenda: SeriableAgenda): Agenda {
    if (agenda.version === "1") {
      return new Agenda({
        id: agenda.id,
        profile_key: agenda.profile_key,
        title: agenda.title,
        fragments: agenda.fragments.map(f => Fragment.deserialize(f)),
        fragment_id_counter: agenda.fragment_id_counter,
      });
    }else{
      throw `invalid agenda: ${agenda}`;
    }
  }


  createUI(callbacks: AgendaUICallbacks): HTMLElement {
    return new AgendaView(this, callbacks).div;
  }


  generateQuery(): Promise<{status: 'okay', query: SessionQuery, warning?: string} | {status: 'error'}> {
    return Promise.allSettled([...this.fragments.values()].map(f => f.generateQuery({profile: this.profile})))
    .then(async results => {
      let queries = [];
      let warnings = [];
      let errors = [];
      
      for (let res of results) {
        if (res.status === 'fulfilled') {
          let value = res.value;
          if (value.type === 'query') {
            queries.push(value.query);
          }else if(value.type === 'warning') {
            queries.push(value.query);
            warnings.push(value.warning);
          }else{
            errors.push(value.error);
          }
        }else{
          errors.push(res.reason);
        }
      }

      if (errors.length !== 0) return { status: 'error' };

      let warning;
      if (warnings.length !== 0) {
        warning = `${warnings.length} warnings`;
      }else{
        warning = undefined;
      }

      let query: SessionQuery = {
        type: 'union',
        subs: queries
      };

      return { status: 'okay', query, warning };
    });
  }
}


class AgendaView {
  div: HTMLElement;
  private agenda: Agenda;
  private callbacks: AgendaUICallbacks;

  private elements: {
    header: {
      txt_title: HTMLInputElement,
      btn_select_profile: HTMLElement,
      btn_show_preview: HTMLElement,
    },

    sidebar: {
      btn_add_new: HTMLElement,
      fragment_list: ReorderableList<number, {title: HTMLElement}>,
    },

    fragment_ui_holder: HTMLElement,

    preview: {
      overlay: Overlay,
      timetable: TimetableView,
      btn_get_link: HTMLElement,
      btn_new_tab: HTMLAnchorElement,
      btn_save_file: HTMLAnchorElement,
    },

    profile_select_overlay: ProfilePickerOverlay
  };

  private currentMainView?: {mode: "preview"} | {mode: "fragment", id: number};
  private currentPreview?: {version: number}

  constructor(agenda: Agenda, callbacks: AgendaUICallbacks) {
    this.div = createElement('div', 'agenda-view');
    this.agenda = agenda;
    this.callbacks = callbacks;
    
    let header = createElement('div', 'header', this.div);
    let sidebar = createElement('div', 'sidebar', this.div);

    let preview = createElement('div', 'preview', this.div);
    let fragment_ui_holder = createElement('div', 'fragment_ui', this.div);

    this.elements = {
      header: {
        txt_title: createElement('input', 'title', header),
        btn_show_preview: createElement('div', 'button show_preview', header),
        btn_select_profile: createElement('div', 'button select_profile', header),
      },

      sidebar: {
        btn_add_new: createElement('div', 'button new', sidebar),
        fragment_list: new ReorderableList({
          serializeId: x => `${x}`,
          deserializeId: x => parseInt(x,10),
          createElement: id => {
            let fragment = this.agenda.fragments.get(id)!;

            let div = createElement('div', 'fragment');
            let title = createElement('div', 'title', div);
            title.innerText = fragment.name ?? fragment.default_name;

            createElement('div', 'selected_indicator', div);
            createElement('div', 'status_indicator', div);
            let btn_delete = createElement('div', 'button delete', div);
            btn_delete.addEventListener('click', (e) => {
              this.deleteFragment(id);
              e.stopPropagation();
            });

            div.addEventListener('click', () => {
              this.setMainView({mode: 'fragment', id});
            });

            return {
              div,
              extra_data: {title},
            };
          },
          reorder_callback: (order, source) => {
            this.agenda.fragment_order = order;
            if (source === 'userInput') {
              callbacks.queryChanged();
            }
          },
        }, {
          extraClasses: ['fragment_list'],
          parent: sidebar,
        })
      },

      fragment_ui_holder,

      preview: {
        btn_get_link: createElement('div', 'button get_link', preview),
        btn_save_file: createElement('a', 'button save_file', preview),
        btn_new_tab: createElement('a', 'button new_tab', preview),
        overlay: new Overlay({parent: preview}),
        timetable: new TimetableView({
          sessionGetter: id => agenda.profile.dataAPI.get_loaded_session(id),
          cellConfig: {
            dateGenerator: (week, day) => agenda.profile.calculate_date(week, day),
            container_type_order: () => agenda.profile.container_types,
          },
        }, {
          parent: preview,
        })
      },
      profile_select_overlay: new ProfilePickerOverlay({parent: this.div})
    };

    // header elements
    this.elements.header.txt_title.placeholder = "Timetable Title";
    this.elements.header.txt_title.value = this.agenda.title || "";
    addOnFinishedTypingListener(this.elements.header.txt_title, () => {
      this.agenda.title = this.elements.header.txt_title.value || undefined;
      this.callbacks.dataChanged();
      if (this.currentMainView?.mode === 'preview') {
        this.setMainView({mode: 'preview'});
      }
    });

    this.elements.header.btn_show_preview.innerText = "Preview";
    this.elements.header.btn_show_preview.addEventListener('click', () => {
      this.setMainView({mode: 'preview'});
    });


    // sidebar elements
    this.elements.sidebar.btn_add_new.addEventListener('click', () => {
      let id = this.newFragment({mode: "new"}, "container_selector", null);
      this.setMainView({mode: "fragment", id});
    });

    // preview
    this.elements.preview.timetable.update();
    this.elements.preview.btn_get_link.innerText = "Copy Link";
    this.elements.preview.btn_new_tab.innerText = "Open in new tab";
    this.elements.preview.btn_new_tab.target = "_blank";
    this.elements.preview.btn_save_file.innerText = "Save to file";

    // fragment
    this.elements.sidebar.fragment_list.setItems(agenda.fragment_order);

    // profile selector
    this.elements.header.btn_select_profile.addEventListener('click', () => {
      this.elements.profile_select_overlay.show(this.agenda.profile_key, (prof_key) => this.setProfile(prof_key))
    })

    this.setProfile(agenda.profile_key);
  }

  private setProfile(profile_key?: ProfileKey) {
    if (profile_key === undefined) return;
    let profile = getProfile(profile_key);
    this.agenda.profile = profile;
    this.agenda.profile_key = profile_key;

    this.elements.header.btn_select_profile.textContent = profileKeyToString(profile_key);

    this.elements.preview.timetable.hours = profile.timetable_hours;
    this.elements.preview.timetable.days = profile.timetable_days;
    this.elements.preview.timetable.updateLayout();

    for (let id of this.agenda.fragment_order) {
      this.generateFragmentQuery(id);
    }
    this.setMainView(this.currentMainView);
    this.setMainView({mode: 'preview'});
  }

  private newFragment<K extends keyof FragmentTypes>(placement: {mode: "replace", id: number} | {mode: "new"}, type: K, data: FragmentTypes[K]["data"]): number {
    let id = this.agenda.fragment_id_counter ++;
    let fragment = Fragment.create(id, type, data);

    this.agenda.fragments.set(id, fragment);

    if (placement.mode === "replace") {
      // get old data
      let old_fragment = this.agenda.fragments.get(placement.id)!;

      fragment.name = old_fragment.name;

      // remove in agenda
      this.agenda.fragments.delete(placement.id);

      // replace in ui
      this.elements.sidebar.fragment_list.replace(placement.id, id);
    }else{
      this.agenda.fragment_order.push(id);
      this.elements.sidebar.fragment_list.append(id);
    }
    this.generateFragmentQuery(id);
    this.callbacks.queryChanged();
    return id;
  }

  private deleteFragment(id: number) {
    if (this.currentMainView?.mode === "fragment" && this.currentMainView?.id === id) {
      this.setMainView();
    }
    this.agenda.fragments.delete(id);
    this.elements.sidebar.fragment_list.delete(id);
    this.callbacks.queryChanged();
  }

  private setMainView(view?: {mode: "preview"} | {mode: "fragment", id: number}) {
    if (this.currentMainView?.mode === 'fragment') {
      let id = this.currentMainView.id;
      this.elements.sidebar.fragment_list.getElementDiv(id)?.classList.remove('selected');
    }
    if (view === undefined) {
      // handled at the end
    }else if (view.mode === "preview") {
      let overlay = this.elements.preview.overlay;

      //clear existing
      let version = (this.currentPreview?.version ?? 0) + 1;
      overlay.clear();

      this.currentPreview = {version};

      let generating_queries = overlay.addMessage("loading", "Generating Queries", {closable: false});

      Promise.allSettled([...this.agenda.fragments.values()].map(f => f.generateQuery({profile: this.agenda.profile})))
      .then(async vs => {
        generating_queries.remove();
        console.log(vs);

        if (this.currentPreview?.version !== version) return;

        let queries: SessionQuery[] = [];
        let errors = [];
        for (let result of vs) {
          if (result.status === "fulfilled") {
            if (result.value.type === "error") {
              errors.push(result.value.error);
            }else{
              queries.push(result.value.query);
            }
          }else{
            errors.push(result.reason);
          }
        }

        if (errors.length > 0) {
          overlay.addMessage("error", "Some errors occurred", {message: JSON.stringify(errors)});
        }

        let combined_query: SessionQuery = {type: 'union', subs: queries};
        let loading = overlay.addMessage("loading", "Loading");
        
        let ids;
        try {
          ids = await this.agenda.profile.dataAPI.query_sessions(combined_query);
        }catch(e) {
          loading.remove();
          console.log(e);
          overlay.addMessage("error", "Error occurred during query", {closable: false, message: JSON.stringify({combined_query, e})});
          return;
        }

        try {
          await this.agenda.profile.dataAPI.load_sessions(ids, {load_referenced_objects: true});

          // place this guard right before setting the timetable content
          if (this.currentPreview?.version !== version) return;

          let timetable = this.elements.preview.timetable;
          timetable.setSessions(ids);
          timetable.update();

          let viewer_params: ViewerParams = {
            type: 'query',
            query: combined_query,
            profile_key: this.agenda.profile_key,
            title: this.agenda.title,
          };

          let link = generateViewerLink(viewer_params);

          this.elements.preview.btn_get_link.onclick = () => {
            navigator.clipboard.writeText(link);
          };
          this.elements.preview.btn_new_tab.href = link;

          this.elements.preview.btn_save_file.setAttribute('href', 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(viewer_params)));
          let filename = viewer_params.title ?? (
            "timetable"
              + (
                  typeof viewer_params.profile_key?.semester === "string"
                  ? " " + viewer_params.profile_key?.semester
                  : "")
              + (
                typeof viewer_params.profile_key?.at === "string"
                ? " v" + viewer_params.profile_key?.at
                : "")
          );
          this.elements.preview.btn_save_file.setAttribute('download', `${filename}.timetable.json`);
        }catch(e) {
          overlay.addMessage("error", "An unexpected error occurred", {closable: false, message: JSON.stringify(e)});
          console.error('unexpected error', e);
        }finally {
          loading.remove();
        }
      });

    }else if(view.mode === "fragment") {
      let id = view.id;
      let fragment = this.agenda.fragments.get(id)!;

      this.elements.sidebar.fragment_list.getElementDiv(id)?.classList.add('selected');
      
      let callbacks: FragmentCallbacks = {
        setDefaultName: () => {
          this.elements.sidebar.fragment_list.getElementData(id)!.title.innerText = fragment.name ?? fragment.default_name;
          this.callbacks.dataChanged();
        },
        dataChanged: () => {
          this.callbacks.dataChanged();
        },
        queryChanged: () => {
          this.generateFragmentQuery(id);
          this.callbacks.queryChanged();
        },
        replaceWith: (type, data) => {
          let newId = this.newFragment({mode: "replace", id}, type, data);
          this.setMainView({mode: "fragment", id: newId});
        },
      };
      let extra_data = {
        profile: this.agenda.profile,
      };
      let div = fragment.createUI(extra_data, callbacks);
      this.elements.fragment_ui_holder.replaceChildren(div);
    }

    this.currentMainView = view;
    if (view?.mode !== undefined) {
      this.div.setAttribute("mode", view.mode);
    }else {
      this.div.removeAttribute("mode");
      removeChildren(this.elements.fragment_ui_holder);
    }
  }

  private generateFragmentQuery(id: number)  {
    let fragment = this.agenda.fragments.get(id)!;

    (async () => {
      let div = this.elements.sidebar.fragment_list.getElementDiv(id)!;

      div.setAttribute("indicator", "loading");

      let query_response = await fragment.generateQuery({profile: this.agenda.profile});

      if (fragment.query_version === query_response.version) {
        if (query_response.type === "query") {
          div.removeAttribute("indicator");
          div.title = '';
        }else{
          div.setAttribute("indicator", query_response.type);
          if (query_response.type === "warning") {
            div.title = query_response.warning;
          }else{
            div.title = query_response.error;
          }
        }
      }
    })();
  }
}
