
import {SessionId, Session, Container} from '../../data';
import { createElement, generateContainerTitle, SYMBOLS } from '../../util';


export type SessionCellConfig = {
  containerViewerLinkGenerator?: (container: Container) => string,
  dateGenerator?: (week: number, day: number) => Date,
  formatter?: (id: SessionId, cell: HTMLElement) => void,
  container_type_order?: () => string[],
};

export type SessionCell = {
  id: SessionId,
  div: HTMLElement,
  timestamp: number | null, // If the timestamp of the session does not match the cached cell, a new cell is created
};

export function createSessionCell(session: Session, config: SessionCellConfig): SessionCell {
  const div = createElement('div', 'sessionCell');
  const subDiv = createElement('div', 'holder', div);
  const cell = {
    id: session.id,
    div: div,
    timestamp: session.last_modified_timestamp,

    subDiv: subDiv,

    header: createHeader(session, config),
    containerSection: createContainerSection(session.containers, config),
    tagSection: createTagSection(session.tags, config),
  };

  div.setAttribute('session_id', `${session.id}`);
  div.setAttribute('type', session.type);


    // header: createElement('div', 'header', subDiv),


    // containerSection: createContainerSection,
    // tagSection: createTagSection(session.tags),

  subDiv.appendChild(cell.header);

  if (cell.containerSection !== undefined) {
    subDiv.appendChild(cell.containerSection);
  }
  if (cell.tagSection !== undefined) {
    subDiv.appendChild(cell.tagSection);
  }


  // const tag_layout

  config.formatter?.(session.id, cell.div);

  return cell;
}

function createHeader(session: Session, config: SessionCellConfig) {
  let {type, weeks, day} = session;

  let div = createElement('div', 'header');
  div.innerText = type;

  let weekString = createWeekString(weeks, day, config);
  if (weekString !== undefined) {
    div.innerText += ', ' + weekString;
  }

  return div;
}

function createWeekString(weeks: number[], day: number, config: SessionCellConfig): string | undefined{
  if (weeks.length === 0) return undefined;

  let ranges: Array<{start: number, end: number}> = [];
  for (let x of [... new Set(weeks)].sort((a,b) => a - b)) {
    if (ranges.length == 0 || ranges[ranges.length -1].end != x - 1) {
      ranges.push({start: x, end: x});
    }else{
      ranges[ranges.length - 1].end = x;
    }
  }

  let numberString;
  if (ranges.length === 1) {
    let {start, end} = ranges[0];
    if (start == end) {
      numberString = `Wk W${start}`;
    }else{
      numberString = `Wks W${start}${SYMBOLS.dash}W${end}`
    }
  }else{
    numberString = `Wks ` + ranges.map(({start, end}) => start == end ? `W${start}`: `W${start}${SYMBOLS.dash}W${end}`).join(', ');
  }

  let dateString;
  if (config.dateGenerator === undefined) {
    dateString = undefined;
  }else{
    let dateToString = (date: Date) => {
      // let options = {
      //   year: '2-digit',
      //   month: 'short',
      //   day: '2-digit',
      // };
      //
      // // @ts-expect-error
      // return date.toISOtoLocaleDateString(undefined, options);

      let MONTHS = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
      ];
      let day = date.getDate();
      let month = date.getMonth();
      let year = date.getFullYear();
      return `${day.toString().padStart(2, '0')}${SYMBOLS.hyphen}${MONTHS[month]}${SYMBOLS.hyphen}${year}`;
    };

    if (ranges.length === 1) {
      let range = ranges[0];
      if (range.start === range.end) {
        dateString = dateToString(config.dateGenerator(range.start, day));
      }else{
        let start = config.dateGenerator(range.start, day);
        let end = config.dateGenerator(range.end, day);
        dateString = `${dateToString(start)} ${SYMBOLS.dash} ${dateToString(end)}`;
      }
    }else{
      let start = config.dateGenerator(ranges[0].start, day);
      let end = config.dateGenerator(ranges[ranges.length - 1].end, day);
      dateString = `${dateToString(start)} ${SYMBOLS.ellipses} ${dateToString(end)}`;
    }
  }

  if (dateString !== undefined) {
    return `${numberString}, ${dateString}`;
  }else{
    return numberString;
  }
}

function createContainerSection(containers: Container[] = [], config: SessionCellConfig): HTMLElement | undefined {
  let div = createElement('div', 'containers');

  function appendCluster(containers: Container[] = []): void {
    if (containers.length === 0) return;
    let holder = createElement('div', 'holder', div);
    for (let c of containers) {
      let element;
      if (config.containerViewerLinkGenerator !== undefined) {
        element = createElement('a', 'container', holder);
        let link = config.containerViewerLinkGenerator(c);
        element.href = link;
        element.target = '_blank';
      }else{
        element = createElement('div', 'container', holder);
      }
      element.title = generateContainerTitle(c);
      element.innerText = c.code;
    }
  }

  if (config.container_type_order !== undefined) {
    let container_by_type = new Map();
    for (let c of containers) {
      let arr = container_by_type.get(c.type);
      if (arr === undefined) {
        arr = [];
        container_by_type.set(c.type, arr);
      }
      arr.push(c);
    }
    for (let type of config.container_type_order()) {
      appendCluster(container_by_type.get(type));
    }
  }else{
    appendCluster(containers);
  }
  if (div.children.length === 0) return undefined;

  return div;
}

type TagFormat = {
  style?: string[],
  prefix?: string,
  sep?: string,
} & ({subs: TagFormat[]} | {tag: string});

const TAG_LAYOUT = {
  style: ['stacked', 'italics'],
  subs: [
    {tag: 'staff', prefix: 'Staff:', style: ['spaces'], sep: ';'},
    {tag: 'mode', prefix: 'Mode:', style: ['spaces'], sep: ','},
    {tag: 'type', style: ['spaces']},
    {
      style: ['spaces2'],
      sep: ';',
      subs:[
        {tag: 'group', style: ['parenthesis', 'spaces']},
        {tag: 'class'},
        {tag: 'notice', style: ['spaces'], sep: ';'},
        {tag: 'contact_notice', style: ['parenthesis', 'spaces']},
        {tag: 'stupid'},
        {tag: 'other'}
      ]
    },
    {tag: 'meta'},
  ],
};

function createTagSection(tags: {[t: string]: string[]}, config: SessionCellConfig): HTMLElement | undefined {
  let inner = createTagSubSection(tags, TAG_LAYOUT, config);
  if (inner === undefined) return;
  let div = createElement('div', 'tags');
  div.append(inner);
  return div;
}
function createTagSubSection(tags: {[t: string]: string[]}, layout: TagFormat, config: SessionCellConfig): HTMLElement | undefined {
  let elements;
  if ("subs" in layout) {// if compound
    elements = layout.subs.map(l => createTagSubSection(tags, l, config)).filter((x): x is HTMLElement => x !== undefined);
  }else if ("tag" in layout) {// tag
    let tag = layout.tag;
    elements = (tags[tag] || []).map(x => {
      let d = createElement('span', 'item');
      d.innerText = x;
      return d;
    });
  }else{
    throw `Invalid tag layout: ${layout}`;
  }

  if (elements.length === 0) return undefined;
  if (layout.sep !== undefined) {
    let sep = layout.sep;
    elements = elements.flatMap(d => {
      let s = createElement('span', 'sep');
      s.innerText = sep;
      return [s,d];
    });
    elements.splice(0,1);
  }

  let div = createElement('span', 'tags', {extraClasses: layout.style || []});

  if (layout.prefix !== undefined) {
    createElement('span', 'prefix', div).innerText = layout.prefix;
  }

  let elements_container = createElement('span', 'container', div);
  for (const element of elements) {
    elements_container.appendChild(element);
  }

  return div;
}
