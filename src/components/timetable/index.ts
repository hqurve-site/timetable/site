import './index.scss';

import * as globals from '../../globals';

import {computeSessionCellPosition, computeStacking} from './util';

import {SessionCell, createSessionCell, SessionCellConfig} from './session_cell';

import {SessionId, Session} from '../../data';
import { createElement, PlacementOptions, removeFromParent } from '../../util';

/*
 *
 *   /---------------------------\
 *   | Corner | Hr0 | Hr1 | .... |
 *   |---------------------------|
 *   | Day 0  |   Contents       |
 *   | Day 1  |   Contents       |
 *   |                           |
 *   \---------------------------/
 *
 *   One massive element which has css-grid.
 *   We use a single container to be able to scroll in both directions
 *
 *   Days stretch the entire width and hours stretch the entire height.
 *   However, contents are stored in day rows
 *
 */

type Config = {
  hours?: number[],
  days?: number[],

  sessionGetter(id: SessionId): Session | undefined,
  cellConfig?: SessionCellConfig,

  autoReflowFilter?: () => boolean,
  cornerCellFormatter?: (cell: HTMLElement) => void,
};

export default class TimetableView {
  private config: Required<Config>;

  div: HTMLElement;
  private grid: HTMLElement;

  private cornerCell: HTMLElement;
  /*
   * Header and Body are directly attached to grid
   * so that we can use the grids alignmnet and easier scrolling.
   * Note that the hourColumns are not directly associated
   * with any particular hour becuase ... it makes stuff simpler
  */
  private hourColumns: Map<number, {header: HTMLElement, body: HTMLElement}>;
  private dayRows: Map<number, {header: HTMLElement, body: HTMLElement}>;

  private sessionIdSet: Set<SessionId> = new Set();

  private sessionCellMap: Map<SessionId, SessionCell> = new Map(); // id -> cell
  // sessionCellInitializer = function(sessionCell){};

  set hours(hours: number[]) {
    this.config.hours = hours;
  }

  set days(days: number[]) {
    this.config.days = days;
  }

  constructor(config: Config, placement?: PlacementOptions ){
    this.config = {
      hours: config.hours ?? globals.HOURS.map(([i,]) => i),
      days: config.days ?? globals.DAYS.map(([i,]) => i),
      sessionGetter: config.sessionGetter,
      cellConfig: config.cellConfig ?? {},
      cornerCellFormatter: config.cornerCellFormatter ?? (() => {}),
      autoReflowFilter: config.autoReflowFilter ?? (() => true),
    };


    this.div = createElement('div', 'timetable-view', placement);

    // create grid 
    this.grid = createElement('div', 'grid', this.div);

    // corner cell
    this.cornerCell = createElement('div', 'cornerCell', this.grid);
    this.config.cornerCellFormatter(this.cornerCell);

    // hour columns
    this.hourColumns = new Map(globals.HOURS.map(([i, hourString]) =>{
      let column = {
        header: createElement('div', 'hourHeader', this.grid),
        body: createElement('div', 'hourBody', this.grid),
      };

      createElement('span', '', column.header).innerText = hourString;

      return [i, column];
    }));

    // dayrows
    this.dayRows = new Map(globals.DAYS.map(([i, {string: dayString}]) => {
      let row = {
        header: createElement('div', 'dayHeader', this.grid),
        body: createElement('div', 'dayBody', this.grid),
      };

      let string_container = createElement('span', '', row.header);
      createElement('span', '', string_container).innerText = dayString;

      return [i, row];
    }));

    this.updateLayout()
    new ResizeObserver(() => {
      if (this.config.autoReflowFilter()) {
        // we throttle the update
        // From https://stackoverflow.com/questions/49384120/resizeobserver-loop-limit-exceeded
        window.requestAnimationFrame(() => this.update());
      }
    }).observe(this.div);
  }

  addSession(id: SessionId) {
    this.sessionIdSet.add(id);
  }
  removeSession(id: SessionId) {
    this.sessionIdSet.delete(id);

    if (this.sessionCellMap.has(id)) {
      let cell = this.sessionCellMap.get(id)!;
      removeFromParent(cell.div);
      this.sessionCellMap.delete(id);
    }
  }
  setSessions(ids: SessionId[]) {
    let idSet = new Set(ids);

    for (let id of [...this.sessionIdSet]) {
      if (idSet.has(id)) continue;
      this.removeSession(id);
    }
    for (let id of ids) {
      this.addSession(id);
    }
  }
  getCell(id: SessionId): HTMLElement | undefined {
    return this.sessionCellMap.get(id)?.div;
  }

  updateLayout() {
    // update hours
    this.grid.style.setProperty('--hour-count', `${this.config.hours.length}`);
    for (let [i, {header, body}] of this.hourColumns) {
      let pos = this.config.hours.indexOf(i);

      header.style.gridColumn = `${pos + 2}`;
      body.style.gridColumn = `${pos + 2}`;

      header.setAttribute('pos', `${pos}`);
      body.setAttribute('pos', `${pos}`);
    }

    // update days
    this.grid.style.setProperty('--day-count', `${this.config.days.length}`);
    for (let [i, {header, body}] of this.dayRows) {
      let pos = this.config.days.indexOf(i);

      header.style.gridRow = `${pos + 2}`;
      body.style.gridRow = `${pos + 2}`;

      header.setAttribute('pos', `${pos}`);
      body.setAttribute('pos', `${pos}`);
    }
  }

  update() {
    console.log('updating');

    let start_time = this.config.hours[0] * 3600;
    let end_time = (this.config.hours[this.config.hours.length - 1] + 1) * 3600;

    // map of index -> array of {id, a, b, height}
    let cellRectangles: Map<number, Array<{id: SessionId, a: number, b: number, height: number}>>
      = new Map(this.config.days.map(i => [i, []]));

    // generate session_cells
    for (let session_id of this.sessionIdSet) {
      let session = this.config.sessionGetter(session_id);
      let session_cell = this.sessionCellMap.get(session_id);

      if (session === undefined) {
        console.log(`missing session ${session_id}`);
        if (session_cell !== undefined) {
          removeFromParent(session_cell.div);
        }
//         this.removeSession(session_id);
        continue;
      }

      if (session_cell === undefined) {
        session_cell = createSessionCell(session, this.config.cellConfig);
        this.sessionCellMap.set(session_id, session_cell);
      }else if(session_cell.timestamp !== session.last_modified_timestamp) {
        removeFromParent(session_cell.div);
        session_cell = createSessionCell(session, this.config.cellConfig);
        this.sessionCellMap.set(session_id, session_cell);
      }

      let position = computeSessionCellPosition(session.timeperiod, start_time, end_time);

      if (position === null) {
        removeFromParent(session_cell.div);
        continue;
      }

      let {start, end} = position;

      if (session_cell.div.parentElement === null) {
        this.dayRows.get(session.day)!.body.appendChild(session_cell.div);
      }

      session_cell.div.style.left = `${start * 100}%`;
      session_cell.div.style.width = `${(end-start) * 100}%`;

      cellRectangles.get(session.day)?.push?.({
        id: session.id,
        a: start,
        b: end,
        height: session_cell.div.clientHeight,
      });
    }

    // position session_cells
    for (let [day, rects] of cellRectangles) {
      let stack_info = computeStacking(rects);

      let height = stack_info.map(x => x.height + x.offset).reduce((a,b) => Math.max(a,b), 0);

      this.dayRows.get(day)!.body.style.minHeight = `${height}px`;

      for (let {id, offset} of stack_info) {
        this.sessionCellMap.get(id)!.div.style.top = `${offset}px`;
      }
    }
  }
};
