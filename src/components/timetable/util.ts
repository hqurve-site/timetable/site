
import {SessionId} from '../../data';

// returns Maybe{start, end}
export function computeSessionCellPosition(
    timeperiod: {start: number, end: number},
    timetable_start: number,
    timetable_end: number,
): {start: number, end: number} | null
{
  let start = (timeperiod.start - timetable_start) / (timetable_end - timetable_start);
  let end   = (timeperiod.end - timetable_start) / (timetable_end - timetable_start);

  return {start, end};
}


function intersects(a: [number, number], b: [number, number]): boolean {
  let [a1, a2] = a;
  let [b1, b2] = b;

  return a1 < b2 && b1 < a2;
}

type StackInput = {id: SessionId, a: number, b: number, height: number};
type StackOutput = StackInput & {offset: number};
export function computeStacking(
    rectangles: StackInput[],
    min=0,
    max=1,
): StackOutput[]
{
  // naively just stack
  // sorted by offset
  let ret: StackOutput[] = [];

  for (let {id, a, b, height} of rectangles) {
    let current = {
      id: id,
      a: a,
      b: b,
      height: height,
      offset: 0
    };

    for (let alloc of ret) {
      if (
        intersects(
          [current.a, current.b],
          [alloc.a, alloc.b],
        )
        && intersects(
          [current.offset, current.offset + current.height],
          [alloc.offset, alloc.offset + alloc.height],
        )
      ) {
        current.offset = alloc.offset + alloc.height;
      }
    }

    let pushed = false;
    for (let [i, alloc] of ret.entries()) {
      if (alloc.offset >= current.offset) {
        ret.splice(i, 0, current);
        pushed = true;
        break;
      }
    }
    if (!pushed) {
      ret.push(current);
    }
  }
  return ret;
}
