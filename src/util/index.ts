import { Container } from "../data";

export type PlacementOptions = {
  extraClasses?: string[],
} & (
  {}
  | { placeholder: HTMLElement }
  | { parent: HTMLElement, position?: "start" | "end" }
);

export function createElement<K extends keyof HTMLElementTagNameMap>(type: K, className?: string, placement?: HTMLElement | PlacementOptions): HTMLElementTagNameMap[K];
export function createElement(type: string, className?: string, placement?: HTMLElement | PlacementOptions): HTMLElement;

export function createElement(type: string, className?: string, placement?: HTMLElement | PlacementOptions) {
  let element = document.createElement(type);

  if (className !== undefined) {
    element.className = className;
  }
  if (placement !== undefined) {
    if (placement instanceof HTMLElement) {
      placement.appendChild(element);
    }else{
      if (placement.extraClasses !== undefined) {
        element.classList.add(... placement.extraClasses);
      }
      if ("placeholder" in placement) {
        element.classList.add(... placement.placeholder.classList);
        placement.placeholder.replaceWith(element);
      }else if ("parent" in placement) {
        if (placement.position === "start" && placement.parent.firstChild !== null) {
          placement.parent.firstChild.before(element);
        }else{
          placement.parent.appendChild(element);
        }
      }
    }
  }
  return element;
}
export function removeFromParent(div: HTMLElement) {
  if (div.parentElement !== null) {
    div.parentElement.removeChild(div);
  }
}
export function removeChildren(div: HTMLElement){
  while(div.lastChild) {
    div.removeChild(div.lastChild);
  }
}

let cachedWidth: undefined | number = undefined;
export function getScollbarWidth() {
  if (cachedWidth !== undefined) return cachedWidth;
  cachedWidth = calcScrollbarWidth();
  return cachedWidth;
}

// strategy from https://github.com/ratiw/vuetable-2/blob/1f01bcf540e3c799dd4fe4d064c9af45784ca481/src/components/Vuetable.vue
function calcScrollbarWidth() {
  const outer = createElement('div');
  const inner = createElement('div', undefined, outer);

  outer.style.visibility = 'hidden';
  outer.style.width = '100px';
  
  inner.style.width= '100%';

  document.body.appendChild(outer);
  
  const widthWithoutScrollbar = outer.offsetWidth;

  outer.style.overflow = 'scroll';

  const widthWithScrollbar = inner.offsetWidth;

  document.body.removeChild(outer);

  return widthWithScrollbar - widthWithoutScrollbar;
}

export function addOnFinishedTypingListener(element: HTMLElement, callback: () => void) {
  const PENDING_TIME = 500;

  let timeoutId: undefined | number = undefined;
  element.addEventListener('keyup', () => {
    if (timeoutId !== undefined) clearTimeout(timeoutId);
    timeoutId = window.setTimeout(() => callback(), PENDING_TIME);
  });
}

export function generateContainerTitle(container: Container): string {
  if (container.name === null) return container.code;
  else return `${container.code} ${SYMBOLS.dash} ${container.name}`;
}

/**
 * This object allows you to essentially cache a single response.
 * The cache is invalidated if the invalidate method is called or a different key is used.
 * 
 * The counter allows you to determine if surrounding infrastructure needs to be updated.
 * 
 * The run method is used to get the data if it does not already exist (for the desired key).
 * Sometimes the data in the initial response is incomplete. In such a case, a data_transform
 * function can be used to transform the initial data. This is triggered using 
 * the perform_change_callback.
 * 
 * Example use case:
 * We have a structure which holds a reference to a container.
 * It searches a given profile for the best matching container.
 * Any changes are reported to the user.
 * The reference is only updated once the user reviews the changes.
 * 
 * - K is the profile
 * - T is the container as well as reported changes
 * - The run method is used to trigger the search for the container if necessary.
 * - The data_transform method is used to update the reference to the container
 *   when the user reviews the changes. It also update wipes the changes from the report (T).
 * 
 * Note that the functionality of data_transform cant be replicated by simply updating the data
 * and then calling the run method again since the profile may have changed between calls to run.
 * The counter must be checked.
 * You can do this manually, or simply use the data_transform method (which checks the counter internally).
 */
export class KeyedPromise<K, T>{
  private _counter: number = 0;
  private inner?: { // contains the current cache state
    key: K,
    data: Promise<T>,
    data_transform?: (t: T) => Promise<T> | T,
  };

  get counter() {
    return this._counter;
  }

  constructor() {}

  invalidate() {
    this.inner = undefined;
    this._counter++; // may be excessive ... not too sure
  }

  /**
   * @param key
   * @param f : function to produce the data. Called with a callback to trigger an update if necessary
   * @param data_transform : update to trigger
   * @returns 
   */
  run(key: K,
      f: (perform_change_callback: () => void, count: number) => Promise<T>,
      data_transform?: (t: T) => Promise<T> | T
  ): Promise<T> {
    if (this.inner === undefined || this.inner.key !== key) {
      this._counter++;
      let count = this._counter; // store to ensure that we have a constant.
      let perform_change_callback = () => {this.perform_change(count);};

      let promise = this.inner?.data ?? Promise.resolve();
      this.inner = {
        key,
        data: promise.then(() => f(perform_change_callback, count)),
        data_transform,
      };
    }
    return this.inner.data;
  }
  private perform_change(count: number) {
    if (this.inner?.data_transform !== undefined && count === this._counter) {
      this.inner.data = this.inner.data.then(this.inner.data_transform);
      this.inner.data_transform = undefined;
    }
  }
}

export class LazyValue<T> {
  private loader: () => Promise<T>;
  private loaded_data?: Promise<T>;

  get isLoaded() {
    return this.loaded_data !== undefined;
  }

  constructor(loader: () => Promise<T> | T) {
    this.loader = async () => loader();
  }
  load() : Promise<T> {
    if (this.loaded_data === undefined) {
      this.loaded_data = this.loader();
    }
    return this.loaded_data;
  }
  then<S>(f: (t: T) => Promise<S> | S): LazyValue<S> {
    return new LazyValue(() => {
      return this.load().then(f);
    })
  }
}

export function rateLimit(f: () => void, waitTime: number= 1, interval:number = 50): () => void {
  let lastRun: undefined | number = undefined;


  let func = () => {
    if (lastRun !== undefined && lastRun > Date.now()) return;

    if (lastRun=== undefined) {
      lastRun = Date.now() + waitTime;
    }else{
      lastRun = Math.max(Date.now() + waitTime, lastRun + interval);
    }
    setTimeout(f, lastRun - Date.now());
  }

  return func;
}

export const PAGE_PATHS = {
  listing: `${ROOT_PATH}/listing`,
  viewer: `${ROOT_PATH}/viewer`,
  builder: `${ROOT_PATH}/builder`,
  root: ROOT_PATH,
};

export const SYMBOLS = {
  // from https://copydashes.com/
  em_dash: "—",
  en_dash: "–",
  minus: "−",
  hyphen: "-",

  dash: "–",

  ellipses: "⋯",
};
