import './index.scss';
import '../../globals.scss';

import {Overlay, TimetableView} from '../../components';
import {SessionId} from '../../data';
import { getProfile, Profile, ProfileKey, profileKeyToString } from '../../globals';
import { generateViewerLink, getPageParams, ViewerParams } from './params';
import { createElement, generateContainerTitle, PAGE_PATHS } from '../../util';
import { ProfilePickerOverlay } from '../../components/profile-picker-overlay';


let overlay = new Overlay({parent: document.body});
window.addEventListener('error', event => {
  overlay.addMessage('error', 'Uncaught error', {
    message: JSON.stringify({
      error: "" + event.error,
      message: event.message,
      filename: event.filename,
      lineno: event.lineno,
      colno: event.colno
    }),
    priority: 100
  });
});

let profile_key: ProfileKey = {};
let profile: Profile;

// setup components
let heading = document.getElementById('heading')!;

// setup timetable
let timetable_fixed_width = false;

let timetable = new TimetableView({
  sessionGetter: id => profile.dataAPI.get_loaded_session(id),
  cellConfig: {
    containerViewerLinkGenerator: container => generateViewerLink({profile_key: profile_key, type: 'container', keys: [container]}),
    dateGenerator: (week, day) => profile.calculate_date(week, day),
    container_type_order: () => profile.container_types,
  },
  autoReflowFilter: () => !timetable_fixed_width,
  cornerCellFormatter: cell => {
    let format = () => {
      if (timetable_fixed_width) {
        timetable.div.style.width = `${timetable.div.clientWidth}px`;
      }else{
        timetable.div.style.width = ``;
      }
    };
    cell.addEventListener('click', () => {
      timetable_fixed_width = !timetable_fixed_width;
      format();
    });
    setTimeout(format, 0);
  },
}, {
  placeholder: document.getElementById('timetable')!,
});

// setup popup for selecting file or pasting params
let user_popup_element = (() => {
  let div = createElement('div', 'popup', {
    placeholder: document.getElementById('popup')!
  });

  let container = createElement('div', 'holder', div);
  let header = createElement('div', 'header', container);
  let footer = createElement('div', 'footer', container);

  let title = createElement('input', 'title', header);
  let btn_profile_selector = createElement('div', 'button select-profile', header);
  let profile_selector = new ProfilePickerOverlay({parent: document.body});
  let text_area = createElement('textarea', 'textarea', container);
  let btn_open_builder = createElement('a', 'button', footer);
  let btn_open_file = createElement('div', 'button open_file', footer);
  let btn_view = createElement('div', 'button view', footer);

  // setup header
  title.placeholder = "Timetable Title"

  // setup buttons
  btn_open_builder.textContent = "Open builder";
  btn_open_builder.href = PAGE_PATHS.builder;
  btn_open_file.textContent = "Load from file";
  btn_view.textContent = "View";

  async function try_populate(string_params: string) {
    let loading_message = overlay.addMessage('loading', 'Loading', {closable: false});
    try {
      let params = JSON.parse(string_params) as ViewerParams;
      if (title.value) {
        params.title = title.value;
      }
      params.profile_key = profile_key;

      // we await to catch any errors
      await populate_page_with_params(params);
      // hide if everything is okay
      div.classList.remove('shown');
    } catch(e){
      overlay.addMessage('error', 'Error', {message: "" + e});
    } finally {
      loading_message.remove();
    }
  }

  btn_profile_selector.innerText = profileKeyToString(profile_key);
  btn_profile_selector.addEventListener('click', () => {
    profile_selector.show(profile_key, (key) =>{
      if (key !== undefined) {
        profile_key = key;
        btn_profile_selector.innerText = profileKeyToString(profile_key);
      }
    })
  });
  btn_view.addEventListener('click', 
    () => try_populate(text_area.value)
  );
  btn_open_file.addEventListener('click', () => {
    // create dummy input and manually call click
    let input = createElement('input');
    input.type = "file";
    input.accept = ".json,text/json";
    
    input.addEventListener('change', async () => {
      try {
        let files = input.files;
        let file = files?.item(0);
        if (files === null || file == null || files.length === 0) {
          overlay.addMessage('error', 'Error', {message: "No files selected"});
        }else if(files.length > 1) {
          overlay.addMessage('error', 'Error', {message: "More than one files selected"});
        }else{
          try_populate(await file.text());
        }
      }catch(e) {
        overlay.addMessage('error', 'Error', {message: "" + e});
      }
    });

    input.click();
  });

  return div;
})()


// setters
function setProfile(p: ProfileKey) {
  profile_key = p;
  profile = getProfile(p);
  timetable.hours = profile.timetable_hours;
  timetable.days = profile.timetable_days;
  timetable.updateLayout();
}
function setTitle(title?: string) {
  heading.innerText = title ?? "";
  document.title = title ?? "Timetable Viewer";
}
async function populate_timetable(ids: SessionId[]) {
  await profile.dataAPI.load_sessions(ids, {load_referenced_objects: true});
  // todo validate sessions obtained
  
  for (let id of ids) {
    timetable.addSession(id);
  }
  timetable.update();
}

// populates the page with parameters
async function populate_page_with_params(params: ViewerParams) {
  setTitle(params.title);

  setProfile(params.profile_key ?? {});

  // resolve options
  if (params.type === "container") {
    let keys = params.keys;

    // get ids and load containers
    let ids = await Promise.all(keys.map(key => profile.dataAPI.query_container_key(key, {load_objects: true})));

    let containers = [];
    for (let [index, id] of ids.entries()) {
      if (id === undefined) throw `missing container "${keys[index]}"`;
      containers.push(profile.dataAPI.get_loaded_container(id)!!);
    }

    if (ids.length === 1) {
      let c = containers[0];
      setTitle(generateContainerTitle(c));
    }else{
      setTitle(containers.map(c => c.code).join(", "));
    }

    await populate_timetable(containers.flatMap(c => c.sessionIds));
  }else {
    let ids = await profile.dataAPI.query_sessions(params.query);
    await populate_timetable(ids);
  }
}

// main function
function main() {
  let loading_message = overlay.addMessage('loading', 'Loading', {closable: false});

  let params;
  try {
    // first try to get page parameters
    params = getPageParams();
    if (params !== null) {
      populate_page_with_params(params);
    }else{
      // if there are no parameters, we show a popup
      user_popup_element.classList.add('shown');
    }
  }catch(e) {
    let handle = overlay.addMessage('error', 'Error', {
      message: "" + e,
      buttons: [ "Manual entry / load from file" ]
    });

    // show popup after showing error
    handle.promise.then(_text => {
      user_popup_element.classList.add('shown');
    })
  } finally {
    loading_message.remove();
  }
}

main()
