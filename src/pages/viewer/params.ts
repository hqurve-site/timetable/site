import { SessionQuery } from "../../data/api";
import { ContainerKey, parseContainerKey, stringifyContainerKey } from "../../data/keys";
import { ProfileKey } from "../../globals";
import { PAGE_PATHS } from "../../util";

type BaseParams = {
  profile_key?: ProfileKey,
  title?: string,
};

type ExtraParams = 
  {type: "container", keys: ContainerKey[]}
  | {type: "query", query: SessionQuery }
;

export type ViewerParams = BaseParams & ExtraParams;

let querySchemes: {[t: string]: {encode(q: string): string, decode(q: string): string}} = {
  '': {encode: q => q, decode: q => q},
  '64': {encode: window.btoa, decode: window.atob},
  'local-storage': {
    encode: function(q: string) : string{
      // generate an id for the form
      //  YYYY-MM-DD-randomnumber
      let date = new Date();

      // Solution to get random number from https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
      // Note getRandomValues is accessible in insecure contexts https://developer.mozilla.org/en-US/docs/Web/API/Crypto/getRandomValues
      let bytes = new Uint8Array(8); // 8 bytes = 16 hex chars
      crypto.getRandomValues(bytes);
      let random = Array.from(bytes, x => x.toString(16).padStart(2,"0")).join('');

      let id = `${date.getFullYear()}-${date.getMonth()}-${date.getDay()}-${random}`
      
      // save in local storage
      localStorage.setItem(`saved_timetable_${id}`, q);

      return id;
    },
    // simply get
    decode: id => {
      let q = localStorage.getItem(`saved_timetable_${id}`);
      if (q == null) {
        throw `No timetable is saved under id "${id}"`
      }
      return q;
    },
  },
};

export function encodeURLParams(params: ViewerParams, urlParams: URLSearchParams = new URLSearchParams()): URLSearchParams {
  if (typeof params.profile_key?.semester === "string") urlParams.append("semester", params.profile_key?.semester);
  if (typeof params.profile_key?.at === "string") urlParams.append("at", params.profile_key?.at);
  if (params.title !== undefined) urlParams.append("title", params.title);

  if (params.type === "container") {
    for (let key of params.keys) {
      urlParams.append("container", stringifyContainerKey(key));
    }
  }else{
    let query = JSON.stringify(params.query);
    console.log(params.query);

    // find the scheme with the shortest length
    let schemes = Object.entries(querySchemes)
    .map(([s, {encode}]) => {
      let q = encode(query);
      return {s, q, length: new URLSearchParams([[s, q]]).toString().length};
    });

    
    let shortest = schemes.filter(({s}) => s !== "local-storage").reduce((prev, curr) => {
      if (prev.length < curr.length) return prev;
      return curr;
    });
    
    // fallback to local-storage if too long
    if (shortest.length > 200) {
      shortest = schemes.find(({s}) => s === "local-storage")!;
    }

    urlParams.append(`q${shortest.s}`, shortest.q);
  }
  return urlParams;
}

function determineParamExtras(urlParams: URLSearchParams): ViewerParams | null{
  if (urlParams.has("container")) {
    let stringified_keys = urlParams.getAll('container');

    let keys = [];
    for (let skey of stringified_keys){
      let key = parseContainerKey(skey);
      if (key === undefined) throw `invalid container key "${skey}"`
      keys.push(key)
    }
    return {type: "container", keys};
  }
  for (let [s, {decode}] of Object.entries(querySchemes)) {
    if (urlParams.has(`q${s}`)) {
      let q = urlParams.get(`q${s}`)!;

      let decoded = decode(q);

      return {type: 'query', query: JSON.parse(decoded)};
    }
  }
  // fallback to nothing
  return null;
}

export function decodeURLParams(urlParams: URLSearchParams): ViewerParams | null {
  let semester = urlParams.get("semester") ?? undefined;
  let at = urlParams.get("at") ?? undefined;
  let profile_key = {semester, at};

  let title = urlParams.get("title") ?? undefined;

  let extra = determineParamExtras(urlParams);
  console.log(extra);

  if (extra === null) return null;

  return {profile_key, title, ...extra};
}

export function generateViewerLink(params: ViewerParams): string {
  return `${PAGE_PATHS.viewer}?${encodeURLParams(params).toString()}`;
}
export function getPageParams() {
  return decodeURLParams( new URLSearchParams(window.location.search));
}
