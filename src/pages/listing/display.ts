import {Container, ContainerId} from '../../data';

import { triggerSave, profile, profile_key } from './index';
import { generateViewerLink } from '../viewer/params';
import { SimpleTable } from '../../components';
import { createElement, generateContainerTitle } from '../../util';

type SortFunction<T> = (a: T, b: T) => number;


function sortHelper<K extends keyof Container>(k: K, f: SortFunction<NonNullable<Container[K]>>): SortFunction<Container> {
  return (_a,_b) => {
    let a = _a[k];
    let b = _b[k];
    if (a === null || a === undefined) return 1;
    if (b === null || b === undefined) return -1;
    return f(a!, b!);
  };
}
let strCmp = (a:string, b: string) => a.toLowerCase().localeCompare(b.toLowerCase());

function factory<K extends keyof Container>(arg: K | ((a: Container) => any)): (a: Container) => HTMLElement {
  let getter: (a: Container) => any;
  if (typeof arg === 'function') {
    getter = arg;
  }else{
    getter = c => c[arg];
  }


  return container => {
    let link = generateViewerLink({profile_key: profile_key, type: 'container', keys: [container]});

    let anchor = createElement('a');
    anchor.innerText = "" + getter(container);
    anchor.href = link;
    return anchor;
  }
}

const elements = {
  btn_clear_sort: document.getElementById("btn_clear_sort")!,
  table_results: new SimpleTable<ContainerId, Container>({
    fallbackSort: (a, b) => a.id - b.id,
    columns: [
      { name: 'type',
        title: 'Type',
        cellFactory: factory('type'),
        sort: sortHelper('type', strCmp),
      },
      { name: 'label',
        title: 'Code / Name',
        cellFactory: factory(a => generateContainerTitle(a)),
        sort: sortHelper('code', strCmp),
      },
      { name: 'nosessions',
        title: '# Sessions',
        cellFactory: factory(a => {
          if (a.sessionIds.length === 1) return '1 Session';
          else return a.sessionIds.length + " Sessions";
        }),
        sort: sortHelper('sessionIds', (a, b) => a.length - b.length),
      },
      { name: 'timestamp',
        title: 'Last Modified',
        cellFactory: factory(a => {
          if (a.last_modified_timestamp === null) return "Unknown";
          else return (new Date(a.last_modified_timestamp * 1000)).toLocaleString();
        }),
        sort: sortHelper('last_modified_timestamp', (a, b) => a - b),
      },
    ],
  }, {
    placeholder: document.getElementById("table_results")!,
    extraClasses: ['table'],
  }),
};

export function init() {
  elements.btn_clear_sort.addEventListener("click", () => {
    clearSort();
    triggerSave();
  });
}

export function clearCache() {
  elements.table_results.clear();
}

export function load_state(_state: any) {}

export function collect_state(){
  return {};
}

function clearSort() {
  elements.table_results.clearSort();
}



export function setContainers(container_ids: ContainerId[]) {
  let containers = [];
  for (let id of container_ids) {
    let container = profile.dataAPI.get_loaded_container(id);
    if (container !== undefined) {
      containers.push(container)
    }
  }
  elements.table_results.setObjects(containers);
}
