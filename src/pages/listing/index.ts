import '../../globals.scss';
import './index.scss';

import { getProfile, Profile, ProfileKey, profileKeyToString } from '../../globals';

import {Overlay} from '../../components';

import * as display from './display';
import * as filter from './filter';
import { ProfilePickerOverlay } from '../../components/profile-picker-overlay';

let urlParams = new URLSearchParams(window.location.search);

export const overlay = new Overlay({ parent: document.body });
export let profile_key: ProfileKey = {}; // default latest
export let profile: Profile;

let elements = {
  select_profile: new ProfilePickerOverlay({
    parent: document.body
  }),
  btn_select_profile: document.getElementById("select_profile")!,

};

main();

async function main() {
  let loading_message = overlay.addMessage('loading', 'Loading page', {closable: false});

  elements.btn_select_profile.addEventListener('click', () => {
    elements.select_profile.show(profile_key, (key) => {
      if (key !== undefined) {
        setProfile(key);
        saveState();
        update();
      }
    })
  });

  filter.init();
  display.init();

  loadState();

  loading_message.remove();

  update();
}

function setProfile(prof_key?: ProfileKey) {
  if (prof_key === undefined) return;

  profile = getProfile(prof_key);
  profile_key = prof_key;
  
  elements.btn_select_profile.textContent = profileKeyToString(prof_key);

  display.clearCache();
  filter.updateProfile();
}

export function triggerSave() {
  saveState();
}

function saveState(){
}
function loadState(){
  setProfile({
    semester: urlParams.get('semester') ?? undefined,
    at: urlParams.get("at") ?? undefined
  });
}

async function update() {
  await filter.update();
}
