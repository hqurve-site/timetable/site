import { ButtonSelect } from '../../components';
import { ContainerQuery } from '../../data/api';
import { addOnFinishedTypingListener } from '../../util';

import { triggerSave, profile, overlay } from './';

import * as display from './display';

let elements = {
  txt_filter: document.getElementById("txt_filter")! as HTMLInputElement,
  btn_search: document.getElementById("btn_search")!,

  type_filter: new ButtonSelect<string>({
    formatter: i => i,
    callback: i => {
      if (elements.type_filter.isSelected(i)) {
        elements.type_filter.removeSelection(i);
      }else{
        elements.type_filter.addSelection(i);
      }
      triggerSave();
      update();
    },
  }, {
    placeholder: document.getElementById("div_type_filter")!,
    extraClasses: [ 'negative' ],
  }),
};

export function init() {
  elements.btn_search.addEventListener("click", () => update());
  elements.txt_filter.addEventListener('keypress', e => {
    if (e.key === 'Enter') update();
  });
  addOnFinishedTypingListener(elements.txt_filter, () => {
    update();
  });
}

export function updateProfile() {
  elements.type_filter.setItems(profile.container_types);
  elements.type_filter.renderAll();
}

let updating = false;
export async function update() {
  if (updating) return;
  updating = true;
  // we keep this so that we can cancel if profile changes midway
  let profile_id = profile.id;

  let search_message = overlay.addMessage('loading', 'Performing search', {closable: false});
  try {
    let ids = await performSearch();
    if (profile.id !== profile_id) return;
    search_message.remove();
    display.setContainers(ids);
  }catch(e) {
    console.error(e);
    search_message.remove();
    await overlay.addMessage('error', 'Search error', {message: "" + e}).promise;
  }
  updating = false;
}

async function performSearch() {
  let query_string = elements.txt_filter.value;

  let query: ContainerQuery;
  query = {type: 'search', string: query_string};

  let hidden_types = elements.type_filter.getSelectedItems();
  if (hidden_types.length !== 0) {
    query = {type: 'container-check', inner: query, checks: hidden_types.map(type => ({type})), negate: true };
  }
  console.log(query);

  let ids = await profile.dataAPI.query_containers(
    query,
    {load_objects: true},
  );

  return ids;
}
