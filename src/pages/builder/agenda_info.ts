import { Agenda, AgendaUICallbacks } from "../../components/agenda";
import { ProfileKey } from "../../globals";
import { LazyValue, rateLimit } from "../../util";


export type SeriableAgendaInfo =
  {
    version: "1",
    id: number,
    summary?: {
      title: string | undefined,
      profile_key: ProfileKey,
    },
    creation_timestamp: number,
    last_edit_timestamp?: number,
  }
;

type Summary = {
  title: string | undefined,
  profile_key: ProfileKey,
};
type Inner = {
  id: number,
  summary?: Summary,
  creation_timestamp: number,
  last_edit_timestamp?: number,
  agenda: LazyValue<Agenda>,
};
export class AgendaInfo {
  private inner: Inner;

  get id() {
    return this.inner.id;
  }
  get summary() {
    return this.inner.summary;
  }

  get creation_timestamp() {
    return this.inner.creation_timestamp;
  }
  get last_edit_timestamp() {
    return this.inner.last_edit_timestamp;
  }

  private constructor(inner: Inner) {
    this.inner = inner;
  }

  static create(id: number): AgendaInfo {
    return new AgendaInfo({
      id,
      creation_timestamp: Date.now(),
      agenda: new LazyValue(() => Agenda.create(id)),
    })
  }
  async clone(id: number, location_prefix: string): Promise<AgendaInfo> {
    // ensure saved
    await this.inner.agenda.load();
    await this.save(location_prefix);

    let info = new AgendaInfo({
      id,
      summary: JSON.parse(JSON.stringify(this.summary)),
      creation_timestamp: Date.now(),
      last_edit_timestamp: this.last_edit_timestamp,
      agenda: AgendaInfo.load(this.id, location_prefix),
    });

    await info.save(location_prefix);
    return info;
  }

  static deserialize(info: SeriableAgendaInfo, location_prefix: string): AgendaInfo {
    if (info.version === "1") {
      let agenda;
      if (info.last_edit_timestamp === undefined) {
        agenda = new LazyValue(() => Agenda.create(info.id));
      }else{
        agenda = AgendaInfo.load(info.id, location_prefix);
      }
      return new AgendaInfo({
        id: info.id,
        summary: info.summary,
        creation_timestamp: info.creation_timestamp,
        last_edit_timestamp: info.last_edit_timestamp,
        agenda,
      });
    }else{
      throw `Invalid info ${info}`;
    }
  }

  serialize(): SeriableAgendaInfo {
    return {
      version: "1",
      id: this.inner.id,
      summary: this.inner.summary,
      creation_timestamp: this.inner.creation_timestamp,
      last_edit_timestamp: this.inner.last_edit_timestamp,
    }
  }

  private static load(id: number, location_prefix: string): LazyValue<Agenda> {
    return new LazyValue(() => {
      let str = localStorage.getItem(`${location_prefix}-${id}`)!;
      return Agenda.deserialize(JSON.parse(str));
    })
  }
  async save(location_prefix: string): Promise<void> {
    if (!this.inner.agenda.isLoaded) return;
    let agenda = await this.inner.agenda.load();
    let str = JSON.stringify(agenda.serialize());
    localStorage.setItem(`${location_prefix}-${agenda.id}`, str);
  }
  
  async createUI(callbacks: AgendaUICallbacks): Promise<HTMLElement> {
    let agenda = await this.inner.agenda.load();

    callbacks = {
      dataChanged: rateLimit(callbacks.dataChanged),
      queryChanged: rateLimit(callbacks.queryChanged),
    };

    return agenda.createUI({
      dataChanged: () => {
        this.inner.last_edit_timestamp = Date.now();
        this.inner.summary = {
          title: agenda.title,
          profile_key: agenda.profile_key,
        };
        callbacks.dataChanged();
      },
      queryChanged: () => {
        this.inner.last_edit_timestamp = Date.now();
        this.inner.summary = {
          title: agenda.title,
          profile_key: agenda.profile_key,
        };
        callbacks.queryChanged();
      },
    })
  }
}
