import './index.scss';

import { Overlay, ReorderableList } from '../../components';
import { AgendaInfo, SeriableAgendaInfo } from './agenda_info';
import { createElement, rateLimit, removeChildren } from '../../util';
import { profileKeyToString } from '../../globals';


let urlParams = new URLSearchParams(window.location.search);

let overlay = new Overlay({placeholder: document.getElementById('overlay')!});
window.addEventListener('unhandledrejection', event => {
  overlay.addMessage('error', 'Unhandled rejection', {
    message: JSON.stringify({
      reason: "" + event.reason,
      promise: "" + event.promise,
    }), 
    priority: 100
  });
});
window.addEventListener('error', event => {
  overlay.addMessage('error', 'Uncaught error', {
    message: JSON.stringify({
      error: "" + event.error,
      message: event.message,
      filename: event.filename,
      lineno: event.lineno,
      colno: event.colno
    }),
    priority: 100
  });
});

setTimeout(main, 0); // just make sure all javascript runs
function main() {
  let state = loadState();
  
  new Controller(state);
}

type State = {
  agendas: Map<number, AgendaInfo>,
  current_agenda: number | undefined,
  agenda_order: number[],
  agenda_id_counter: number,
};
type SerializableState = 
  { // unusable due to different method of storing profiles
    version: "1" | "2",
    agendas: SeriableAgendaInfo[],
    current_agenda: number,
  }
| {
  version: "3",
    agendas: SeriableAgendaInfo[],
    current_agenda: number | undefined,
    agenda_id_counter: number,
  }
;

const AGENDA_INFO_PREFIX = "timetable-builder-agenda-#";

function loadState(): State {
  let string = window.localStorage.getItem("timetable-builder");

  if (string === null) {
    return {
      agendas: new Map(),
      current_agenda: undefined,
      agenda_order: [],
      agenda_id_counter: 0,
    };
  }else{
    let state = JSON.parse(string) as SerializableState;

    if (state.version === "1" || state.version === "2") {
      overlay.addMessage('warning', 'Incompatible saved data', {
        message: 'Clearing saved data since your old agendas are no longer compatible (and I don\'t feel like adding functionality to upgrade it).',
        closable: true,
        timeout: Infinity,
      });
      return {
        agendas: new Map(),
        current_agenda: undefined,
        agenda_order: [],
        agenda_id_counter: 0,
      };
    }else if (state.version === "3") {
      return {
        agendas: new Map(state.agendas.map(a => [a.id, AgendaInfo.deserialize(a, AGENDA_INFO_PREFIX)] )),
        agenda_order: state.agendas.map(a => a.id),
        current_agenda: state.current_agenda,
        agenda_id_counter: state.agenda_id_counter,
      }
    }else {
      throw `Invalid state ${state}`;
    }
  }
}

async function saveState(state: State): Promise<void> {
  let agendas = state.agenda_order.map(aid => state.agendas.get(aid)!);
  let serializable: SerializableState = {
    version: "3",
    agendas: agendas.map(a => a.serialize()),
    current_agenda: state.current_agenda,
    agenda_id_counter: state.agenda_id_counter
  };
  window.localStorage.setItem("timetable-builder", JSON.stringify(serializable));
  await Promise.allSettled(agendas.map(a => a.save(AGENDA_INFO_PREFIX)));
}



class Controller{
  private inner: {
    state: State,
    save_callback(): void,
  };

  private elements: {
    agenda_holder: HTMLElement,
    
    popup: {
      div: HTMLElement,
      agenda_list: ReorderableList<number, {refreshInfo(): void}>
    },
  }

  constructor(state: State) {
    this.inner = {
      state,
      save_callback: rateLimit(() => saveState(state)),
    }

    let popupDiv = document.getElementById('popup')!;
    let popupHolder = createElement('div', 'holder', popupDiv);
    let popupBtnNew = createElement('div', 'button new', popupHolder);
    popupBtnNew.innerText = "New";
    popupBtnNew.addEventListener('click', () => this.newAgenda().then(id => this.showAgenda(id)));
    popupDiv.addEventListener('click', event => {
      if (event.target === popupDiv){
        this.hidePopup();
      }
    });

    
    this.elements = {
      agenda_holder: document.getElementById('body')!,
      popup: {
        div: popupDiv,
        agenda_list: new ReorderableList({
          drag_identifier: 'agenda-list',
          serializeId: i => `${i}`,
          deserializeId: i => parseInt(i, 10),
          reorder_callback: (ids, source) => {
            this.inner.state.agenda_order = ids;
            if (source === "userInput") {
              this.inner.save_callback();
            }
          },
          empty_message: 'There are no agendas. Please create one.',
          createElement: id => {
            let info = this.inner.state.agendas.get(id)!;

            let div = createElement('div', 'item');

            let main = createElement('div', 'main', div);
            let extra = createElement('div', 'extra', div);

            // setup main
            let handle = createElement('div', 'handle', main);
            let title = createElement('div', 'title', main);
            let profile = createElement('div', 'profile', main);
            let lastModified = createElement('div', 'last_modified', main);
            let creationTime = createElement('div', 'creation_time', main);
            let expandButton = createElement('div', 'expand', main);

            expandButton.addEventListener('click', event => {
              event.stopPropagation();
              if (div.classList.contains('expanded')) {
                div.classList.remove('expanded');
              }else{
                div.classList.add('expanded');
              }
            });
            main.addEventListener('click', () => this.showAgenda(id) );

            // setup expanded
            let del = createElement('div', 'delete button', extra);
            let clone = createElement('div', 'clone button', extra);
            del.innerText = "Delete";
            clone.innerText = "Duplicate";
            del.addEventListener('click', event => {
              event.stopPropagation();
              this.deleteAgenda(id)
            });
            clone.addEventListener('click', event => {
              event.stopPropagation();
              this.newAgenda(id);
            });
            
            let refreshInfo = () => {
              if (info.summary === undefined) {
                div.classList.remove('initiated');
                title.innerText = `Untitled ${id}`;
              }else{
                div.classList.add('initiated');
                title.innerText = info.summary.title ?? `Untitled ${id}`;
                
                profile.textContent = profileKeyToString(info.summary.profile_key);
              }
              
              creationTime.innerText = new Date(info.creation_timestamp).toLocaleString();
              if (info.last_edit_timestamp === undefined) {
                lastModified.innerText = "Never modified";
              }else{
                lastModified.innerText = new Date(info.last_edit_timestamp).toLocaleString();
              }
            };
            refreshInfo();

            return {
              div,
              extra_data: {refreshInfo},
            };
          },
        },{
          parent: popupHolder,
          extraClasses: ['agenda_list'],
        }),
      },
    }

    this.elements.popup.agenda_list.setItems(state.agenda_order);

    document.getElementById('btn_show_popup')!.addEventListener('click', () => this.showPopup());

    if (state.current_agenda === undefined) {
      this.showPopup();
    }else{
      this.showAgenda(state.current_agenda);
    }
  }


  // nothing too bad can happen here since we are just waiting on the UI which itself causes little change
  private async showAgenda(id?: number) {
    if (this.inner.state.current_agenda !== undefined) {
      this.elements.popup.agenda_list.getElementDiv(this.inner.state.current_agenda)?.classList.remove('selected');
    }
    this.inner.state.current_agenda = id;
    this.hidePopup();
    if (id === undefined) {
      removeChildren(this.elements.agenda_holder);
    }else {
      let loading = overlay.addMessage('loading', 'Loading Agenda', {closable: false});
      let agenda = this.inner.state.agendas.get(id)!;

      let popupInfo = this.elements.popup.agenda_list.getElementData(id)!;

      let div = await agenda.createUI({
        dataChanged: () => {
          popupInfo.refreshInfo();
          this.inner.save_callback();
        },
        queryChanged: () => {
          popupInfo.refreshInfo();
          this.inner.save_callback();
        },
      });
      this.elements.agenda_holder.replaceChildren(div);
      this.elements.popup.agenda_list.getElementDiv(id)?.classList.add('selected');
      loading.remove();
    }
    this.inner.save_callback();
  }

  private hidePopup() {
    if (this.inner.state.current_agenda === undefined) return;
    this.elements.popup.div.classList.remove('shown');
  }
  private showPopup() {
    this.elements.popup.div.classList.add('shown');
  }

  private async newAgenda(source?: number): Promise<number> {
    let state = this.inner.state;
    let id = ++state.agenda_id_counter;

    let loading = overlay.addMessage('loading', source === undefined ? 'Creating new agenda' : 'Copying agenda', {closable: false});

    let agenda;
    if (source === undefined){
      agenda = AgendaInfo.create(id);
    }else{
      let oldAgenda = state.agendas.get(source)!;
      agenda = await oldAgenda.clone(id, AGENDA_INFO_PREFIX);
    }

    state.agendas.set(id, agenda);
    this.elements.popup.agenda_list.append(id);
    this.inner.save_callback();

    loading.remove();

    return id;
  }

  private async deleteAgenda(id: number) {
    let agenda = this.inner.state.agendas.get(id)!;

    let title = agenda.summary?.title ?? `Untitled Agenda ${id}`;
    let response = await overlay.addMessage(
      'warning',
      `Delete ${title}?`,
      {
        message: `Are you sure you want to delete "${title}"?`,
        buttons: ['yes', 'no'],
      },
    ).promise;

    if (response !== 'yes') return;

    this.elements.popup.agenda_list.delete(id);
    this.inner.save_callback();

    if (id === this.inner.state.current_agenda) {
      this.showAgenda(undefined);
    }
  }
}
