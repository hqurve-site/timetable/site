// path with the following
//   - info.json
//   - timetables
//      - <profile_id>.json

import * as _info from './config/info.json';
import { StaticDataAPI, DataAPI } from './data/api';
import { LazyValue } from './util';

const config = (_info as unknown) as {
  days: Array<[number, {offset: number, string: string}]>,
  profiles: Array<{
    id: string,
    type: 'static',

    publish_timestamp: number, // in milliseconds
    semester: string,

    start_date: {
      year: number,
      month: number, // not zero indexed (ie June is 6)
      day: number,
    },
    container_types: Array<string>,

    timetable_days: number[],
    timetable_hours: number[],
  }>,
};

// There are four modes:
// - nothing:         latest
// - semester only:   latest in semester
// - at only:         latest profile published at or before specified time
// - semester and at: latest in semester published at or before the specified time
export type ProfileKey = {
  semester?: string,
  at?: string,
}
export type Profile = {
  id: string,

  publish_date: Date,
  semester: string,

  dataAPI: DataAPI,

  start_date: Date,
  container_types: Array<string>,

  timetable_days: number[],
  timetable_hours: number[],

  calculate_date: (week: number, day: number) => Date,
};

export const DAYS = config.days;
export const DAYS_MAP = new Map(DAYS);

// [hour(0..23), representation as string]
export const HOURS: Array<[number, string]> = new Array(24).fill(0)
  .map((_, h) => [h, `${(h + 11) % 12 +1} ${h >= 12 ? "PM" : "AM"}`]);

// sorted from oldest to newest
export const PROFILE_LIST = config.profiles.map(profile => {
  let id = profile.id;

  // @ts-expect-error
  let dataAPI: DataAPI = undefined;

  let start_date = new Date(profile.start_date.year, profile.start_date.month - 1, profile.start_date.day);

  let prof: Profile = {
    id: profile.id,

    publish_date: new Date(profile.publish_timestamp),
    semester: profile.semester,
    
    dataAPI,

    start_date,
    container_types: profile.container_types,

    timetable_days: profile.timetable_days,
    timetable_hours: profile.timetable_hours,
    calculate_date: (week, day) => {
      let date = new Date(start_date);
      date.setDate(date.getDate() + week * 7 + DAYS_MAP.get(day)!.offset);
      return date;
    },
  };

  if (profile.type === 'static') {
    prof.dataAPI = new StaticDataAPI(prof, new LazyValue(
      () => fetch(`${ROOT_PATH}/_/timetables/${id}.json`).then(r => r.json())
    ));
  }else{
    throw `Unknown content type for ${id}: ${profile.type}`;
  }

  return prof;
}).sort((a,b) => a.publish_date.getTime() - b.publish_date.getTime());


export function getProfile(key: ProfileKey): Profile {
  let {semester, at} = key;

  // first find the profiles with matching semester
  let filtered = PROFILE_LIST.filter(p => p.semester === semester);
  // if there are none, just use all
  if (filtered.length === 0) {
    filtered = PROFILE_LIST;
  }

  // next find the oldest which is not after
  let date = at !== undefined ? new Date(at) : new Date();

  // search from newest to oldest
  // Note that this has the sideeffect of invalid dates being considered as newest
  let index = filtered.length - 1;
  while (index > 0 && date < filtered[index].publish_date) {
    index--;
  }

  return filtered[index]
}

export function profileKeyToString(key: ProfileKey): string {
  if (key.semester !== undefined) {
    if (key.at !== undefined) {
      return `${key.semester} (${key.at})`;
    }else{
      return `${key.semester}`;
    }
  }else {
    if (key.at !== undefined) {
      return `${key.at}`;
    }else{
      return `latest (${getProfile({}).semester})`;
    }
  }
}

// export function calc_date(day, week) {
//   let date = new Date(START_DATE);
//   date.setDate(date.getDate() + DAYS.get(day).offset + week * 7);
//   return date;
// }
