
declare module '*.json' {
  const content: unknown;
  export default content;
}

// Path to root. Does not end in /
declare const ROOT_PATH: string;
