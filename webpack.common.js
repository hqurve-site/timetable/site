const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin')


const PAGES = Object.entries({
  viewer: {path: 'viewer', title: 'Viewer'},
  listing: {path: 'listing', title: 'Listing'},
  builder: {path: 'builder', title: 'Builder'},
  root: {path: '.', title: 'Timetable Site'},
}).map(([name, {path, title}]) => {
  return {
    name,
    title,
    path,
    entry: `./src/pages/${name}/index.ts`,
    pageTemplate: `./src/pages/${name}/index.hbs`,
  };
});


module.exports = {
  entry: {
    ... Object.fromEntries(PAGES.map(({name, entry}) => [name, entry]))
  },
  output: {
    filename: '_/[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    assetModuleFilename: '_/[hash][ext][query]',
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.scss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          // 'style-loader',
          'css-loader',
          {
            loader: 'resolve-url-loader',
          },
          {
            loader: 'sass-loader',
            options: {sourceMap: true}
          },
        ],
      },
      {
        test: /\.svg$/,
        type: 'asset/inline',
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.hbs$/,
        loader: 'handlebars-loader',
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "_/[name].css",
    }),
    new CopyPlugin({
      patterns: [
        {
          from: "*",
          to: "_/timetables",
          context: "src/config/timetables"
        }
      ]
    }),

    ... PAGES.map(({title, name, pageTemplate, path}) => new HtmlWebpackPlugin({
      title,
      filename: `${path}/index.html`,
      chunks: [name],
      template: pageTemplate,
      templateParameters: {
        ROOT_PATH: path === '.' ? '.' : path.split('/').map(() => '..').join('/'),
      },
    })),
  ],
};
